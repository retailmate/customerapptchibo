package com.cognizant.iot.productdetails.model;

/**
 * Created by 599584 on 2/22/2017.
 */

public class ColorModel {

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    int color;
}
