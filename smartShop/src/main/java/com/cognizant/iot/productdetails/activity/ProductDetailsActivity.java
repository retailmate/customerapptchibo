package com.cognizant.iot.productdetails.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.cognizant.iot.Network.VolleyHelper;
import com.cognizant.iot.Network.VolleyRequest;
import com.cognizant.iot.homepage.ProductSearchAPI;
import com.cognizant.iot.homepage.models.productsearch.ProductSearchModel;
import com.cognizant.iot.homepage.models.recommendation.RecommendationModel;
import com.cognizant.iot.productdetails.adapter.ColorAdapter;
import com.cognizant.iot.productdetails.adapter.ProductReviewRecyclerviewAdapter;
import com.cognizant.iot.productdetails.adapter.SuggestedProductRecyclerAdapter;
import com.cognizant.iot.productdetails.adapter.ViewPagerAdapter;
import com.cognizant.iot.productdetails.model.ColorModel;
import com.cognizant.iot.productdetails.model.ProductModel;
import com.cognizant.iot.productdetails.model.ReviewModel;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.iot.utils.Constants;
import com.cognizant.retailmate.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductDetailsActivity extends AppCompatActivity {
//    public List<IconData1> data1 = new ArrayList<>();

    public List<ColorModel> colorList = new ArrayList<>();

    Toolbar toolbar;
    ImageView image1;
    //    TextView r_count, title;
    CollapsingToolbarLayout collapsingToolbarLayout;
    ColorAdapter colorAdapter;
    RecyclerView recyclerView, review;
    SuggestedProductRecyclerAdapter recyclerAdapter1;
    ProductReviewRecyclerviewAdapter productReviewRecyclerviewAdapter;
    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;


    List<ProductModel> productList = new ArrayList<>();

    AppBarLayout appBarLayout;

    List<ReviewModel> reviewList = new ArrayList<>();
    private RecyclerView ColorRecycler;


    ProductSearchModel productSearchModel;

    String prodid;
    String prodname;
    String prodprice;
    String flag;
    String beacon;
    String desc;
    String position, category;

    TextView prodname_tv, pdt_cost;

    ImageView info_button, back_button, minus, plus;

    Boolean imageLoading = false;

    TextView productQty;

    int min = 1, smin = 1;
    int max = 10, smax = 10;
    int Sval;

    Integer valqty, valQ, valsize, valS;
    ImageView cart;
    ImageView wish_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_productdetails);


        prodname_tv = (TextView) findViewById(R.id.product_name);
        ImageView product_image = (ImageView) findViewById(R.id.productImage);
        TextView category_text_view = (TextView) findViewById(R.id.category);
        wish_btn = (ImageView) findViewById(R.id.wishlist);
        pdt_cost = (TextView) findViewById(R.id.productPrice);
        cart = (ImageView) findViewById(R.id.cart);
        TextView offer_details_tv = (TextView) findViewById(R.id.productOffer);

        info_button = (ImageView) findViewById(R.id.info_button);
        back_button = (ImageView) findViewById(R.id.back_button);

        minus = (ImageView) findViewById(R.id.minus);
        plus = (ImageView) findViewById(R.id.plus);

        productQty = (TextView) findViewById(R.id.productQty);

        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        appBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbar);

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                Log.e("##$$", String.valueOf(collapsingToolbarLayout.getHeight() + verticalOffset) + " ** " + String.valueOf(2 * ViewCompat.getMinimumHeight(collapsingToolbarLayout)));
                if (collapsingToolbarLayout.getHeight() + verticalOffset < 80) {
                    prodname_tv.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.green));
                } else {
                    prodname_tv.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                }
            }
        });


        String res2 = productQty.getText().toString();
        valsize = Integer.parseInt(res2);
        Sval = valsize.intValue();


        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (Sval <= smax - 1) {
                    Sval = Sval + 1;
                    valS = new Integer(Sval);

                    productQty.setText(valS.toString());
                }
            }
        });


        // decrement size
        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (Sval >= smin + 1) {
                    Sval = Sval - 1;
                    valS = new Integer(Sval);

                    productQty.setText(valS.toString());
                }
            }
        });


        Intent i = getIntent();
        // Get the result of prodid
        prodid = i.getStringExtra("prodid");
        // Get the result of prodname
        prodname = i.getStringExtra("prodname");
        // Get the result of prodprice
        prodprice = i.getStringExtra("prodprice");
        // Get the result of flag
        flag = i.getStringExtra("flag");
        desc = i.getStringExtra("desc");

        System.out.println("@@## DESCRIPTION DETAIL PAGE" + desc);
        beacon = i.getStringExtra("beacon");

        category = i.getStringExtra("category");

        collapsingToolbarLayout.setTitle(prodname);
        prodname_tv.setText(prodname);
        pdt_cost.setText(prodprice);
        category_text_view.setText(category);


        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (R.drawable.cart_red_icon == cart.getId()) {
                    cart.setImageResource(R.drawable.cart_grey);
                } else {
                    cart.setImageResource(R.drawable.cart_red_icon);
                }

                List<String> list = new ArrayList<String>();
                list.add(prodid);
                ProductSearchAPI productSearchAPI = new ProductSearchAPI(ProductDetailsActivity.this, list, "cart");

                productSearchAPI.makeaCall();

            }
        });

        wish_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (R.drawable.hearticonnn == wish_btn.getId()) {
                    wish_btn.setImageResource(R.drawable.heart_grey);
                } else {
                    wish_btn.setImageResource(R.drawable.hearticonnn);
                }

                List<String> list = new ArrayList<String>();
                list.add(prodid);
                ProductSearchAPI productSearchAPI = new ProductSearchAPI(ProductDetailsActivity.this, list, "wishlist");

                productSearchAPI.makeaCall();
            }
        });

        if (AccountState.getOfflineMode()) {
            Picasso.with(this).load(getResources().getIdentifier(flag, "drawable", getPackageName())).into(product_image);
        } else {
//        Picasso.with(getApplicationContext()).load("http://travelfashiongirl.com/wp-content/uploads/2014/02/How-to-Clean-Your-Travel-Backpack.jpg").into(product_image);
            Picasso.with(getApplicationContext()).load("https://landmarkdevret.cloudax.dynamics.com/MediaServer/" + flag).into(product_image);
        }
        product_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (AccountState.getOfflineMode()) {
                    Toast.makeText(ProductDetailsActivity.this, "Go online for more images", Toast.LENGTH_SHORT).show();
                } else {
                    if (imageLoading) {
                        System.out.println("@@## Recommed product response" + productSearchModel.getProductSearchValueModel().get(0));
                        Intent act2 = new Intent(view.getContext(), ProductDetailImagesActivity.class);
                        act2.putExtra("imageModel", (Serializable) productSearchModel.getProductSearchValueModel().get(0).getImage());
                        startActivity(act2);
                    }
                }
            }
        });
        //LinearLayoutManager layoutManager  = new GridLayoutManager(getApplicationContext(),  2);

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        LinearLayoutManager layoutManagerColor
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        ColorRecycler = (RecyclerView) findViewById(R.id.colorlist);
        colorAdapter = new ColorAdapter(getApplicationContext(), colorList);
        ColorRecycler.setAdapter(colorAdapter);
        ColorRecycler.setLayoutManager(layoutManagerColor);


        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);


        recyclerView.setLayoutManager(layoutManager);

        populateData();


        NestedScrollView scrollView = (NestedScrollView) findViewById(R.id.scrollView);
        scrollView.setFillViewport(true);

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        viewPager = (ViewPager) findViewById(R.id.viewPager);


        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragments(new UserReviewFragment(), "My Circle Review (" + String.valueOf(reviewList.size()) + ")");
        viewPagerAdapter.addFragments(new UserReviewFragment("users"), "User Review");

        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);


        recyclerAdapter1 = new SuggestedProductRecyclerAdapter(getApplicationContext(), productList);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(recyclerAdapter1);

        if (AccountState.getOfflineMode()) {

        } else {
            productsearchcall(prodid);
        }

    }

    private void populateData() {

        ProductModel model = new ProductModel();
        model.setName("Full sleeve Shirt");
        model.setPic(R.drawable.productjson2);
        model.setCategory("Shirts");
        model.setPrice((float) 65.89);
        productList.add(model);

        model = new ProductModel();
        model.setName("Privat Kaffee");
        model.setPic(R.drawable.productjson4);
        model.setCategory("Coffee");
        model.setPrice((float) 7.0);
        productList.add(model);


        ReviewModel rmodel = new ReviewModel();

        rmodel.setName("Jack Gibbs");
        rmodel.setDate("12/12/2016");
        rmodel.setReview("I love this Bag!");
        reviewList.add(rmodel);

        rmodel = new ReviewModel();
        rmodel.setName("Mary Gibbs");
        rmodel.setDate("1/2/2017");
        rmodel.setReview("I love this Bag too!");
        reviewList.add(rmodel);


        ColorModel colorModel = new ColorModel();
        colorModel.setColor(R.color.colorPrimaryDark);
        colorList.add(colorModel);

        colorModel = new ColorModel();
        colorModel.setColor(R.color.black);
        colorList.add(colorModel);

        colorModel = new ColorModel();
        colorModel.setColor(R.color.green);
        colorList.add(colorModel);

        colorModel = new ColorModel();
        colorModel.setColor(R.color.black);
        colorList.add(colorModel);

        colorModel = new ColorModel();
        colorModel.setColor(R.color.colorPrimary);
        colorList.add(colorModel);

        colorModel = new ColorModel();
        colorModel.setColor(R.color.colorPrimaryLight);
        colorList.add(colorModel);
    }


    public void productsearchcall(String prodid) {

        String[] products = new String[1];

        products[0] = prodid;
        System.out.println("@@## Recommed product ID" + products[0]);

        Map<String, String> headers = new HashMap<>();
        JSONObject finalObject = null;
        try {

            headers.put("Content-Type", "application/json");
            headers.put("Authorization", "id_token " + AccountState.getTokenID());
            Log.e("TAG", "AccountState.getTokenID()" + AccountState.getTokenID());
            headers.put(
                    "OUN",
                    "094");

            JSONArray parentData = new JSONArray();

            for (int i = 0; i < products.length; i++) {


                parentData.put(Double.valueOf(products[i]));
            }

            JSONObject childData = new JSONObject();

            childData.put("Ids", parentData);
            childData.put("DataLevelValue", 4);

            finalObject = new JSONObject();
            finalObject.put("productSearchCriteria", childData);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        VolleyRequest<ProductSearchModel> recommendationproductdetailVolleyRequest =
                new VolleyRequest<ProductSearchModel>(Request.Method.POST, Constants.PRODUCT_SEARCH_BY_ID_API, ProductSearchModel.class, headers, finalObject,

                        new com.android.volley.Response.Listener<ProductSearchModel>() {
                            @Override
                            public void onResponse(ProductSearchModel response) {
                                Log.e("Recom product details \n", "" + response);
                                System.out.println("@@## Recommed product response" + response.getProductSearchValueModel().get(0));
                                productSearchModel = response;
                                imageLoading = true;
                                Toast.makeText(ProductDetailsActivity.this, "Images loaded", Toast.LENGTH_SHORT).show();
                            }
                        }
                        //API CALL GETTING MADE FOR Dynamic Version contd

                        , new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null && networkResponse.statusCode != 200) {
                            Log.e("TAG", "No network on update.");
                            Toast.makeText(ProductDetailsActivity.this, "Network Error, please check internet", Toast.LENGTH_SHORT).show();

                        }

                    }
                });


        RetryPolicy policy = new DefaultRetryPolicy(VolleyHelper.Timeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        recommendationproductdetailVolleyRequest.setRetryPolicy(policy);
        VolleyHelper.getInstance(ProductDetailsActivity.this).addToRequestQueue(recommendationproductdetailVolleyRequest);


    }


    void recommendationProductDetailsApiCall(RecommendationModel recommendationModel) {

        String[] products = new String[(recommendationModel.getValue().size())];
        for (int i = 0; i < recommendationModel.getValue().size(); i++) {
            products[i] = recommendationModel.getValue().get(i).getProductId();

        }


        Map<String, String> headers = new HashMap<>();
        JSONObject finalObject = null;
        try {

            headers.put("Content-Type", "application/json");
            headers.put("Authorization", "id_token " + AccountState.getTokenID());
            Log.e("TAG", "AccountState.getTokenID()" + AccountState.getTokenID());
            headers.put(
                    "OUN",
                    "094");

            JSONArray parentData = new JSONArray();

            for (int i = 0; i < products.length; i++) {


                parentData.put(Double.valueOf(products[i]));
            }

            JSONObject childData = new JSONObject();

            childData.put("Ids", parentData);
            childData.put("DataLevelValue", 4);

            finalObject = new JSONObject();
            finalObject.put("productSearchCriteria", childData);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        VolleyRequest<ProductSearchModel> recommendationModelVolleyRequest =
                new VolleyRequest<ProductSearchModel>(Request.Method.POST, Constants.PRODUCT_SEARCH_BY_ID_API, ProductSearchModel.class, headers, finalObject,

                        new com.android.volley.Response.Listener<ProductSearchModel>() {
                            @Override
                            public void onResponse(ProductSearchModel response) {
                                Log.e("Your Recommendations \n", "" + response.getProductSearchValueModel().size());

                            }
                        }
                        //API CALL GETTING MADE FOR Dynamic Version contd

                        , new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse != null && networkResponse.statusCode != 200) {
                            Log.e("TAG", "No network on update.");
                            Toast.makeText(ProductDetailsActivity.this, "Network Error, please check internet", Toast.LENGTH_SHORT).show();

                        }

                    }
                });


        RetryPolicy policy = new DefaultRetryPolicy(VolleyHelper.Timeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        recommendationModelVolleyRequest.setRetryPolicy(policy);
        VolleyHelper.getInstance(getApplicationContext()).addToRequestQueue(recommendationModelVolleyRequest);
    }

}