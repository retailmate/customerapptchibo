package com.cognizant.iot.productdetails.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cognizant.iot.productdetails.model.ProductModel;
import com.cognizant.retailmate.R;

import java.util.List;


/**
 * Created by 599654 on 2/17/2017.
 */

public class SuggestedProductRecyclerAdapter extends RecyclerView.Adapter<SuggestedProductRecyclerAdapter.MyViewHolder> {

    List<ProductModel> productList;


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView name, category, price;
        public ImageView pic;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.prodname);
            category = (TextView) view.findViewById(R.id.category);
            price = (TextView) view.findViewById(R.id.prodprice);
            pic = (ImageView) view.findViewById(R.id.flag);

        }
    }

    public SuggestedProductRecyclerAdapter(Context context, List<ProductModel> productList) {
        this.productList = productList;
    }

    @Override
    public SuggestedProductRecyclerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.productdetails_item_details_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SuggestedProductRecyclerAdapter.MyViewHolder holder, int position) {

        ProductModel model = productList.get(position);

        holder.name.setText(model.getName());
        holder.category.setText(model.getCategory());
        holder.price.setText( String.valueOf(model.getPrice()));
        holder.pic.setImageResource(model.getPic());

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }
}
