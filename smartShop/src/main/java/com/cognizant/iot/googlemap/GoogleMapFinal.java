package com.cognizant.iot.googlemap;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.retailmate.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class GoogleMapFinal extends AppCompatActivity {
    public String value = "";
    Intent myIntent = getIntent();
    static double latitude;
    static double longitude;
    LatLng retailpoint;
    private GoogleMap googleMaps;
    TextView name;
    Marker[] option = new Marker[2];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//		getActionBar().setDisplayHomeAsUpEnabled(true);
//		getActionBar().setHomeButtonEnabled(true);
//		getActionBar().setDisplayShowTitleEnabled(true);
//		getActionBar().setBackgroundDrawable(
//				new ColorDrawable(Color.parseColor("#0577D9")));
//		getActionBar().setIcon(
//				new ColorDrawable(getResources().getColor(
//						android.R.color.transparent)));
//
//		int titleId = getResources().getIdentifier("action_bar_title", "id",
//				"android");
//
//		TextView abTitle = (TextView) findViewById(titleId);
//		abTitle.setTextColor(Color.parseColor("#ffffff"));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.google_final_activity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarGoogleMapFinal);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);


        Toast.makeText(getApplicationContext(),
                "Showing location specific retail mates", Toast.LENGTH_SHORT)
                .show();
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            value = extras.getString("PlaceName");
            latitude = extras.getDouble("latitude");
            longitude = extras.getDouble("longitude");
            Toast.makeText(getApplicationContext(),
                    "Location of Retail Mate at " + value, Toast.LENGTH_SHORT)
                    .show();
            name = (TextView) findViewById(R.id.google_map_final_tv);
            name.setText("Location of retail mate at " + value);
        }

        try {
            // Loading map
            initilizeMap();

            // Changing map type
            googleMaps.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            // Showing / hiding your current location
            // googleMap.setMyLocationEnabled(true);

            // Enable / Disable zooming controls
            googleMaps.getUiSettings().setZoomControlsEnabled(false);

            // Enable / Disable my location button
            googleMaps.getUiSettings().setMyLocationButtonEnabled(true);

            // Enable / Disable Compass icon
            googleMaps.getUiSettings().setCompassEnabled(true);

            // Enable / Disable Rotate gesture
            googleMaps.getUiSettings().setRotateGesturesEnabled(true);

            // Enable / Disable zooming functionality
            googleMaps.getUiSettings().setZoomGesturesEnabled(true);

            retailpoint = new LatLng(latitude, longitude);

            option[0] = googleMaps.addMarker(new MarkerOptions().position(
                    retailpoint).title("Retail Mate at " + value));
            option[0].setIcon(BitmapDescriptorFactory
                    .defaultMarker(BitmapDescriptorFactory.HUE_GREEN));

            LatLng myLocation = callToGetMyLocation();
            option[1] = googleMaps.addMarker(new MarkerOptions().position(
                    myLocation).title("My Location"));
            option[1].setIcon(BitmapDescriptorFactory
                    .defaultMarker(BitmapDescriptorFactory.HUE_RED));

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(retailpoint).zoom(10).build();
            googleMaps.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));
        } catch (Exception e) {
            e.printStackTrace();
        }

		/*
         * try { if (googleMaps == null) { googleMaps = ((MapFragment)
		 * getFragmentManager
		 * ().findFragmentById(R.id.map_fragment_final)).getMap(); }
		 * 
		 * final LatLng retailpoint = new LatLng(latitude , longitude);
		 * googleMaps.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
		 * 
		 * option[0] = googleMaps.addMarker(new
		 * MarkerOptions().position(retailpoint
		 * ).title("Retail Mate at "+value));
		 * option[0].setIcon(BitmapDescriptorFactory
		 * .defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
		 * 
		 * LatLng myLocation=callToGetMyLocation();
		 * option[1]=googleMaps.addMarker(new
		 * MarkerOptions().position(myLocation).title("My Location"));
		 * option[1].
		 * setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory
		 * .HUE_RED));
		 * 
		 * googleMaps.setMyLocationEnabled(true);
		 * googleMaps.getUiSettings().setZoomControlsEnabled(true);
		 * googleMaps.getUiSettings().setCompassEnabled(true);
		 * googleMaps.getUiSettings().setMyLocationButtonEnabled(true);
		 * googleMaps.getUiSettings().setRotateGesturesEnabled(true); } catch
		 * (Exception e) { e.printStackTrace(); }
		 */
    }

    private void initilizeMap() {
        if (googleMaps == null) {
            googleMaps = ((MapFragment) getFragmentManager().findFragmentById(
                    R.id.map_fragment_final)).getMap();

            if (googleMaps == null) {
                Toast.makeText(getApplicationContext(),
                        "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    public LatLng callToGetMyLocation() {
        LatLng myPosition;
        double latitude, longitude;
        googleMaps.setMyLocationEnabled(true);

        // Getting LocationManager object from System Service LOCATION_SERVICE
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        // Creating a criteria object to retrieve provider
        Criteria criteria = new Criteria();

        // Getting the name of the best provider
        String provider = locationManager.getBestProvider(criteria, true);

        // Getting Current Location
        Location location = locationManager.getLastKnownLocation(provider);

        // if(location!=null)

        // Getting latitude of the current location
        latitude = location.getLatitude();

        // Getting longitude of the current location
        longitude = location.getLongitude();

        // Creating a LatLng object for the current location
        // LatLng latLng = new LatLng(latitude, longitude);\

        myPosition = new LatLng(latitude, longitude);

        return myPosition;
    }

    @Override
    public void onResume() {
        super.onResume();
        initilizeMap();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title

        // Handle action bar actions click
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
