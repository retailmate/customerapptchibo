package com.cognizant.iot.homepage.adapter;

/**
 * Created by 599584 on 2/17/2017.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cognizant.iot.homepage.fragments.RecyclerViewHolders;
import com.cognizant.iot.homepage.models.productlist.ProductListValueModel;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.retailmate.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ProductListRecyclerAdapter extends RecyclerView.Adapter<RecyclerViewHolders> {
    private List<ProductListValueModel> itemList;
    public Context context;
    String categoryname;

    public ProductListRecyclerAdapter(Context context, List<ProductListValueModel> itemList, String categoryName) {
        this.itemList = itemList;
        this.context = context;
        this.categoryname = categoryName;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.homepage_cardviewlist, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView, context, itemList);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {

        Log.e("##$$", "#$#$#$#" + String.valueOf(position));


        holder.productname.setText(itemList.get(position).getName());

        holder.prodprice.setText(itemList.get(position).getPrice().toString());
        holder.category.setText(categoryname);
        if (AccountState.getOfflineMode()) {
            Picasso.with(context).load(context.getResources().getIdentifier(itemList.get(position).getPrimaryImageUrl(), "drawable", context.getPackageName())).into(holder.Photo);
        } else {
            Picasso.with(context).load("https://landmarkdevret.cloudax.dynamics.com/MediaServer/" + itemList.get(position).getPrimaryImageUrl()).into(holder.Photo);
        }
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }


}
