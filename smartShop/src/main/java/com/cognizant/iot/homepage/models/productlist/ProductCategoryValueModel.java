package com.cognizant.iot.homepage.models.productlist;

/**
 * Created by 452781 on 2/21/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductCategoryValueModel {

    @SerializedName("RecordId")
    @Expose
    private String recordId;
    @SerializedName("OfflineImage")
    @Expose
    private Object offlineImage;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("ParentCategory")
    @Expose
    private String parentCategory;
    @SerializedName("Images")
    @Expose
    private List<ProductCategoryImageModel> images = null;
    @SerializedName("NameTranslations")
    @Expose
    private List<ProductCategoryNameTranslationModel> nameTranslations = null;
    @SerializedName("ExtensionProperties")
    @Expose
    private List<Object> extensionProperties = null;

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public Object getOfflineImage() {
        return offlineImage;
    }

    public void setOfflineImage(Object offlineImage) {
        this.offlineImage = offlineImage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(String parentCategory) {
        this.parentCategory = parentCategory;
    }

    public List<ProductCategoryImageModel> getImages() {
        return images;
    }

    public void setImages(List<ProductCategoryImageModel> images) {
        this.images = images;
    }

    public List<ProductCategoryNameTranslationModel> getNameTranslations() {
        return nameTranslations;
    }

    public void setNameTranslations(List<ProductCategoryNameTranslationModel> nameTranslations) {
        this.nameTranslations = nameTranslations;
    }

    public List<Object> getExtensionProperties() {
        return extensionProperties;
    }

    public void setExtensionProperties(List<Object> extensionProperties) {
        this.extensionProperties = extensionProperties;
    }

}