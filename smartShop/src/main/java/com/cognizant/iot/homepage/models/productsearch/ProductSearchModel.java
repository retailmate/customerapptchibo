package com.cognizant.iot.homepage.models.productsearch;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by 452781 on 2/21/2017.
 */
public class ProductSearchModel implements Serializable {


    @SerializedName("@odata.context")
    @Expose
    private String odataContext;
    @SerializedName("value")
    @Expose
    private List<ProductSearchValueModel> value = null;

    public String getOdataContext() {
        return odataContext;
    }

    public void setOdataContext(String odataContext) {
        this.odataContext = odataContext;
    }

    public List<ProductSearchValueModel> getProductSearchValueModel() {
        return value;
    }

    public void setProductSearchValueModel(List<ProductSearchValueModel> value) {
        this.value = value;
    }

}
