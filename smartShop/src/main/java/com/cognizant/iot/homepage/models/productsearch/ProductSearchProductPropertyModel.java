package com.cognizant.iot.homepage.models.productsearch;

/**
 * Created by 452781 on 2/21/2017.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ProductSearchProductPropertyModel implements Serializable {

    @SerializedName("ValueString")
    @Expose
    private String valueString;
    @SerializedName("PropertyTypeValue")
    @Expose
    private Integer propertyTypeValue;
    @SerializedName("KeyName")
    @Expose
    private String keyName;
    @SerializedName("FriendlyName")
    @Expose
    private String friendlyName;
    @SerializedName("RecordId")
    @Expose
    private Integer recordId;
    @SerializedName("IsDimensionProperty")
    @Expose
    private Boolean isDimensionProperty;
    @SerializedName("AttributeValueId")
    @Expose
    private Integer attributeValueId;
    @SerializedName("UnitText")
    @Expose
    private Object unitText;
    @SerializedName("GroupId")
    @Expose
    private Integer groupId;
    @SerializedName("GroupTypeValue")
    @Expose
    private Integer groupTypeValue;
    @SerializedName("GroupName")
    @Expose
    private String groupName;
    @SerializedName("ExtensionProperties")
    @Expose
    private List<Object> extensionProperties = null;
    @SerializedName("ProductId")
    @Expose
    private Integer productId;
    @SerializedName("CatalogId")
    @Expose
    private Integer catalogId;

    public String getValueString() {
        return valueString;
    }

    public void setValueString(String valueString) {
        this.valueString = valueString;
    }

    public Integer getPropertyTypeValue() {
        return propertyTypeValue;
    }

    public void setPropertyTypeValue(Integer propertyTypeValue) {
        this.propertyTypeValue = propertyTypeValue;
    }

    public String getKeyName() {
        return keyName;
    }

    public void setKeyName(String keyName) {
        this.keyName = keyName;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public void setFriendlyName(String friendlyName) {
        this.friendlyName = friendlyName;
    }

    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }

    public Boolean getIsDimensionProperty() {
        return isDimensionProperty;
    }

    public void setIsDimensionProperty(Boolean isDimensionProperty) {
        this.isDimensionProperty = isDimensionProperty;
    }

    public Integer getAttributeValueId() {
        return attributeValueId;
    }

    public void setAttributeValueId(Integer attributeValueId) {
        this.attributeValueId = attributeValueId;
    }

    public Object getUnitText() {
        return unitText;
    }

    public void setUnitText(Object unitText) {
        this.unitText = unitText;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Integer getGroupTypeValue() {
        return groupTypeValue;
    }

    public void setGroupTypeValue(Integer groupTypeValue) {
        this.groupTypeValue = groupTypeValue;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<Object> getExtensionProperties() {
        return extensionProperties;
    }

    public void setExtensionProperties(List<Object> extensionProperties) {
        this.extensionProperties = extensionProperties;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(Integer catalogId) {
        this.catalogId = catalogId;
    }

}