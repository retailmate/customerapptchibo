package com.cognizant.iot.homepage.models.cart;

/**
 * Created by 452781 on 2/23/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CartDatamodel {

    @SerializedName("AffiliationLines")
    @Expose
    private List<Object> affiliationLines = null;
    @SerializedName("IsRequiredAmountPaid")
    @Expose
    private Boolean isRequiredAmountPaid;
    @SerializedName("IsDiscountFullyCalculated")
    @Expose
    private Boolean isDiscountFullyCalculated;
    @SerializedName("AmountDue")
    @Expose
    private Double amountDue;
    @SerializedName("AmountPaid")
    @Expose
    private Integer amountPaid;
    @SerializedName("AttributeValues")
    @Expose
    private List<Object> attributeValues = null;
    @SerializedName("BeginDateTime")
    @Expose
    private String beginDateTime;
    @SerializedName("BusinessDate")
    @Expose
    private Object businessDate;
    @SerializedName("CancellationChargeAmount")
    @Expose
    private Object cancellationChargeAmount;
    @SerializedName("EstimatedShippingAmount")
    @Expose
    private Object estimatedShippingAmount;
    @SerializedName("CartLines")
    @Expose
    private List<CartLineModel> cartLines = null;
    @SerializedName("CartTypeValue")
    @Expose
    private Integer cartTypeValue;
    @SerializedName("ChannelId")
    @Expose
    private String channelId;
    @SerializedName("ChargeAmount")
    @Expose
    private Integer chargeAmount;
    @SerializedName("ChargeLines")
    @Expose
    private List<Object> chargeLines = null;
    @SerializedName("TaxViewLines")
    @Expose
    private List<Object> taxViewLines = null;
    @SerializedName("Comment")
    @Expose
    private String comment;
    @SerializedName("InvoiceComment")
    @Expose
    private String invoiceComment;
    @SerializedName("CustomerId")
    @Expose
    private String customerId;
    @SerializedName("CustomerOrderModeValue")
    @Expose
    private Integer customerOrderModeValue;
    @SerializedName("DeliveryMode")
    @Expose
    private Object deliveryMode;
    @SerializedName("DeliveryModeChargeAmount")
    @Expose
    private Object deliveryModeChargeAmount;
    @SerializedName("DiscountAmount")
    @Expose
    private Double discountAmount;
    @SerializedName("DiscountCodes")
    @Expose
    private List<Object> discountCodes = null;
    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("TransactionTypeValue")
    @Expose
    private Integer transactionTypeValue;
    @SerializedName("CustomerAccountDepositLines")
    @Expose
    private List<Object> customerAccountDepositLines = null;
    @SerializedName("IncomeExpenseLines")
    @Expose
    private List<Object> incomeExpenseLines = null;
    @SerializedName("IncomeExpenseTotalAmount")
    @Expose
    private Integer incomeExpenseTotalAmount;
    @SerializedName("IsReturnByReceipt")
    @Expose
    private Boolean isReturnByReceipt;
    @SerializedName("ReturnTransactionHasLoyaltyPayment")
    @Expose
    private Boolean returnTransactionHasLoyaltyPayment;
    @SerializedName("IsFavorite")
    @Expose
    private Boolean isFavorite;
    @SerializedName("IsRecurring")
    @Expose
    private Boolean isRecurring;
    @SerializedName("IsSuspended")
    @Expose
    private Boolean isSuspended;
    @SerializedName("LoyaltyCardId")
    @Expose
    private String loyaltyCardId;
    @SerializedName("ModifiedDateTime")
    @Expose
    private String modifiedDateTime;
    @SerializedName("Name")
    @Expose
    private Object name;
    @SerializedName("OrderNumber")
    @Expose
    private Object orderNumber;
    @SerializedName("AvailableDepositAmount")
    @Expose
    private Integer availableDepositAmount;
    @SerializedName("OverriddenDepositAmount")
    @Expose
    private Object overriddenDepositAmount;
    @SerializedName("PrepaymentAmountPaid")
    @Expose
    private Integer prepaymentAmountPaid;
    @SerializedName("PrepaymentAppliedOnPickup")
    @Expose
    private Integer prepaymentAppliedOnPickup;
    @SerializedName("PromotionLines")
    @Expose
    private List<Object> promotionLines = null;
    @SerializedName("QuotationExpiryDate")
    @Expose
    private Object quotationExpiryDate;
    @SerializedName("ReasonCodeLines")
    @Expose
    private List<Object> reasonCodeLines = null;
    @SerializedName("ReceiptEmail")
    @Expose
    private Object receiptEmail;
    @SerializedName("RequestedDeliveryDate")
    @Expose
    private Object requestedDeliveryDate;
    @SerializedName("RequiredDepositAmount")
    @Expose
    private Integer requiredDepositAmount;
    @SerializedName("SalesId")
    @Expose
    private Object salesId;
    @SerializedName("ShippingAddress")
    @Expose
    private Object shippingAddress;
    @SerializedName("StaffId")
    @Expose
    private Object staffId;
    @SerializedName("SubtotalAmount")
    @Expose
    private Double subtotalAmount;
    @SerializedName("SubtotalAmountWithoutTax")
    @Expose
    private Double subtotalAmountWithoutTax;
    @SerializedName("TaxAmount")
    @Expose
    private Double taxAmount;
    @SerializedName("TaxOnCancellationCharge")
    @Expose
    private Integer taxOnCancellationCharge;
    @SerializedName("TaxOverrideCode")
    @Expose
    private Object taxOverrideCode;
    @SerializedName("TenderLines")
    @Expose
    private List<Object> tenderLines = null;
    @SerializedName("TerminalId")
    @Expose
    private String terminalId;
    @SerializedName("TotalAmount")
    @Expose
    private Double totalAmount;
    @SerializedName("TotalManualDiscountAmount")
    @Expose
    private Integer totalManualDiscountAmount;
    @SerializedName("TotalManualDiscountPercentage")
    @Expose
    private Integer totalManualDiscountPercentage;
    @SerializedName("WarehouseId")
    @Expose
    private String warehouseId;
    @SerializedName("IsCreatedOffline")
    @Expose
    private Boolean isCreatedOffline;
    @SerializedName("CartStatusValue")
    @Expose
    private Integer cartStatusValue;
    @SerializedName("ReceiptTransactionTypeValue")
    @Expose
    private Integer receiptTransactionTypeValue;
    @SerializedName("ExtensionProperties")
    @Expose
    private List<Object> extensionProperties = null;

    public List<Object> getAffiliationLines() {
        return affiliationLines;
    }

    public void setAffiliationLines(List<Object> affiliationLines) {
        this.affiliationLines = affiliationLines;
    }

    public Boolean getIsRequiredAmountPaid() {
        return isRequiredAmountPaid;
    }

    public void setIsRequiredAmountPaid(Boolean isRequiredAmountPaid) {
        this.isRequiredAmountPaid = isRequiredAmountPaid;
    }

    public Boolean getIsDiscountFullyCalculated() {
        return isDiscountFullyCalculated;
    }

    public void setIsDiscountFullyCalculated(Boolean isDiscountFullyCalculated) {
        this.isDiscountFullyCalculated = isDiscountFullyCalculated;
    }

    public Double getAmountDue() {
        return amountDue;
    }

    public void setAmountDue(Double amountDue) {
        this.amountDue = amountDue;
    }

    public Integer getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(Integer amountPaid) {
        this.amountPaid = amountPaid;
    }

    public List<Object> getAttributeValues() {
        return attributeValues;
    }

    public void setAttributeValues(List<Object> attributeValues) {
        this.attributeValues = attributeValues;
    }

    public String getBeginDateTime() {
        return beginDateTime;
    }

    public void setBeginDateTime(String beginDateTime) {
        this.beginDateTime = beginDateTime;
    }

    public Object getBusinessDate() {
        return businessDate;
    }

    public void setBusinessDate(Object businessDate) {
        this.businessDate = businessDate;
    }

    public Object getCancellationChargeAmount() {
        return cancellationChargeAmount;
    }

    public void setCancellationChargeAmount(Object cancellationChargeAmount) {
        this.cancellationChargeAmount = cancellationChargeAmount;
    }

    public Object getEstimatedShippingAmount() {
        return estimatedShippingAmount;
    }

    public void setEstimatedShippingAmount(Object estimatedShippingAmount) {
        this.estimatedShippingAmount = estimatedShippingAmount;
    }

    public List<CartLineModel> getCartLineModels() {
        return cartLines;
    }

    public void setCartLineModels(List<CartLineModel> cartLines) {
        this.cartLines = cartLines;
    }

    public Integer getCartTypeValue() {
        return cartTypeValue;
    }

    public void setCartTypeValue(Integer cartTypeValue) {
        this.cartTypeValue = cartTypeValue;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public Integer getChargeAmount() {
        return chargeAmount;
    }

    public void setChargeAmount(Integer chargeAmount) {
        this.chargeAmount = chargeAmount;
    }

    public List<Object> getChargeLines() {
        return chargeLines;
    }

    public void setChargeLines(List<Object> chargeLines) {
        this.chargeLines = chargeLines;
    }

    public List<Object> getTaxViewLines() {
        return taxViewLines;
    }

    public void setTaxViewLines(List<Object> taxViewLines) {
        this.taxViewLines = taxViewLines;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getInvoiceComment() {
        return invoiceComment;
    }

    public void setInvoiceComment(String invoiceComment) {
        this.invoiceComment = invoiceComment;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public Integer getCustomerOrderModeValue() {
        return customerOrderModeValue;
    }

    public void setCustomerOrderModeValue(Integer customerOrderModeValue) {
        this.customerOrderModeValue = customerOrderModeValue;
    }

    public Object getDeliveryMode() {
        return deliveryMode;
    }

    public void setDeliveryMode(Object deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    public Object getDeliveryModeChargeAmount() {
        return deliveryModeChargeAmount;
    }

    public void setDeliveryModeChargeAmount(Object deliveryModeChargeAmount) {
        this.deliveryModeChargeAmount = deliveryModeChargeAmount;
    }

    public Double getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(Double discountAmount) {
        this.discountAmount = discountAmount;
    }

    public List<Object> getDiscountCodes() {
        return discountCodes;
    }

    public void setDiscountCodes(List<Object> discountCodes) {
        this.discountCodes = discountCodes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getTransactionTypeValue() {
        return transactionTypeValue;
    }

    public void setTransactionTypeValue(Integer transactionTypeValue) {
        this.transactionTypeValue = transactionTypeValue;
    }

    public List<Object> getCustomerAccountDepositLines() {
        return customerAccountDepositLines;
    }

    public void setCustomerAccountDepositLines(List<Object> customerAccountDepositLines) {
        this.customerAccountDepositLines = customerAccountDepositLines;
    }

    public List<Object> getIncomeExpenseLines() {
        return incomeExpenseLines;
    }

    public void setIncomeExpenseLines(List<Object> incomeExpenseLines) {
        this.incomeExpenseLines = incomeExpenseLines;
    }

    public Integer getIncomeExpenseTotalAmount() {
        return incomeExpenseTotalAmount;
    }

    public void setIncomeExpenseTotalAmount(Integer incomeExpenseTotalAmount) {
        this.incomeExpenseTotalAmount = incomeExpenseTotalAmount;
    }

    public Boolean getIsReturnByReceipt() {
        return isReturnByReceipt;
    }

    public void setIsReturnByReceipt(Boolean isReturnByReceipt) {
        this.isReturnByReceipt = isReturnByReceipt;
    }

    public Boolean getReturnTransactionHasLoyaltyPayment() {
        return returnTransactionHasLoyaltyPayment;
    }

    public void setReturnTransactionHasLoyaltyPayment(Boolean returnTransactionHasLoyaltyPayment) {
        this.returnTransactionHasLoyaltyPayment = returnTransactionHasLoyaltyPayment;
    }

    public Boolean getIsFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(Boolean isFavorite) {
        this.isFavorite = isFavorite;
    }

    public Boolean getIsRecurring() {
        return isRecurring;
    }

    public void setIsRecurring(Boolean isRecurring) {
        this.isRecurring = isRecurring;
    }

    public Boolean getIsSuspended() {
        return isSuspended;
    }

    public void setIsSuspended(Boolean isSuspended) {
        this.isSuspended = isSuspended;
    }

    public String getLoyaltyCardId() {
        return loyaltyCardId;
    }

    public void setLoyaltyCardId(String loyaltyCardId) {
        this.loyaltyCardId = loyaltyCardId;
    }

    public String getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(String modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public Object getName() {
        return name;
    }

    public void setName(Object name) {
        this.name = name;
    }

    public Object getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Object orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Integer getAvailableDepositAmount() {
        return availableDepositAmount;
    }

    public void setAvailableDepositAmount(Integer availableDepositAmount) {
        this.availableDepositAmount = availableDepositAmount;
    }

    public Object getOverriddenDepositAmount() {
        return overriddenDepositAmount;
    }

    public void setOverriddenDepositAmount(Object overriddenDepositAmount) {
        this.overriddenDepositAmount = overriddenDepositAmount;
    }

    public Integer getPrepaymentAmountPaid() {
        return prepaymentAmountPaid;
    }

    public void setPrepaymentAmountPaid(Integer prepaymentAmountPaid) {
        this.prepaymentAmountPaid = prepaymentAmountPaid;
    }

    public Integer getPrepaymentAppliedOnPickup() {
        return prepaymentAppliedOnPickup;
    }

    public void setPrepaymentAppliedOnPickup(Integer prepaymentAppliedOnPickup) {
        this.prepaymentAppliedOnPickup = prepaymentAppliedOnPickup;
    }

    public List<Object> getPromotionLines() {
        return promotionLines;
    }

    public void setPromotionLines(List<Object> promotionLines) {
        this.promotionLines = promotionLines;
    }

    public Object getQuotationExpiryDate() {
        return quotationExpiryDate;
    }

    public void setQuotationExpiryDate(Object quotationExpiryDate) {
        this.quotationExpiryDate = quotationExpiryDate;
    }

    public List<Object> getReasonCodeLines() {
        return reasonCodeLines;
    }

    public void setReasonCodeLines(List<Object> reasonCodeLines) {
        this.reasonCodeLines = reasonCodeLines;
    }

    public Object getReceiptEmail() {
        return receiptEmail;
    }

    public void setReceiptEmail(Object receiptEmail) {
        this.receiptEmail = receiptEmail;
    }

    public Object getRequestedDeliveryDate() {
        return requestedDeliveryDate;
    }

    public void setRequestedDeliveryDate(Object requestedDeliveryDate) {
        this.requestedDeliveryDate = requestedDeliveryDate;
    }

    public Integer getRequiredDepositAmount() {
        return requiredDepositAmount;
    }

    public void setRequiredDepositAmount(Integer requiredDepositAmount) {
        this.requiredDepositAmount = requiredDepositAmount;
    }

    public Object getSalesId() {
        return salesId;
    }

    public void setSalesId(Object salesId) {
        this.salesId = salesId;
    }

    public Object getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(Object shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public Object getStaffId() {
        return staffId;
    }

    public void setStaffId(Object staffId) {
        this.staffId = staffId;
    }

    public Double getSubtotalAmount() {
        return subtotalAmount;
    }

    public void setSubtotalAmount(Double subtotalAmount) {
        this.subtotalAmount = subtotalAmount;
    }

    public Double getSubtotalAmountWithoutTax() {
        return subtotalAmountWithoutTax;
    }

    public void setSubtotalAmountWithoutTax(Double subtotalAmountWithoutTax) {
        this.subtotalAmountWithoutTax = subtotalAmountWithoutTax;
    }

    public Double getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(Double taxAmount) {
        this.taxAmount = taxAmount;
    }

    public Integer getTaxOnCancellationCharge() {
        return taxOnCancellationCharge;
    }

    public void setTaxOnCancellationCharge(Integer taxOnCancellationCharge) {
        this.taxOnCancellationCharge = taxOnCancellationCharge;
    }

    public Object getTaxOverrideCode() {
        return taxOverrideCode;
    }

    public void setTaxOverrideCode(Object taxOverrideCode) {
        this.taxOverrideCode = taxOverrideCode;
    }

    public List<Object> getTenderLines() {
        return tenderLines;
    }

    public void setTenderLines(List<Object> tenderLines) {
        this.tenderLines = tenderLines;
    }

    public String getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(String terminalId) {
        this.terminalId = terminalId;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getTotalManualDiscountAmount() {
        return totalManualDiscountAmount;
    }

    public void setTotalManualDiscountAmount(Integer totalManualDiscountAmount) {
        this.totalManualDiscountAmount = totalManualDiscountAmount;
    }

    public Integer getTotalManualDiscountPercentage() {
        return totalManualDiscountPercentage;
    }

    public void setTotalManualDiscountPercentage(Integer totalManualDiscountPercentage) {
        this.totalManualDiscountPercentage = totalManualDiscountPercentage;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public Boolean getIsCreatedOffline() {
        return isCreatedOffline;
    }

    public void setIsCreatedOffline(Boolean isCreatedOffline) {
        this.isCreatedOffline = isCreatedOffline;
    }

    public Integer getCartStatusValue() {
        return cartStatusValue;
    }

    public void setCartStatusValue(Integer cartStatusValue) {
        this.cartStatusValue = cartStatusValue;
    }

    public Integer getReceiptTransactionTypeValue() {
        return receiptTransactionTypeValue;
    }

    public void setReceiptTransactionTypeValue(Integer receiptTransactionTypeValue) {
        this.receiptTransactionTypeValue = receiptTransactionTypeValue;
    }

    public List<Object> getExtensionProperties() {
        return extensionProperties;
    }

    public void setExtensionProperties(List<Object> extensionProperties) {
        this.extensionProperties = extensionProperties;
    }

}