package com.cognizant.iot.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.iot.activity.CatalogActivity;
import com.cognizant.iot.activityfindproduct.JSONFunctionsDetails_findprod;
import com.cognizant.iot.activityproductlist.ImageLoaderprod;
import com.cognizant.iot.activityproductlist.JSONfunctions_deletefrmwishlist_prod;
import com.cognizant.iot.activityproductlist.MainActivityprod;
import com.cognizant.iot.activityproductlist.Product_description_productscreen;
import com.cognizant.iot.activitywishlist.JSONfunctions_wish_deleteproduct;
import com.cognizant.iot.mycart.JsonFuntionCart;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.retailmate.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.Header;

public class ListViewAdapter_prod_full extends BaseAdapter {

    ProgressDialog mProgressDialog;
    JSONObject jsonobject;
    JSONArray jsonarray;

    String prodid1;
    ProgressDialog prgDialog;
    Context context;
    LayoutInflater inflater;

    ArrayList<HashMap<String, String>> arraylist;
    ArrayList<HashMap<String, String>> data;
    ArrayList<HashMap<String, String>> initialDisplayedValues;

    HashMap<String, String> resultp = new HashMap<String, String>();
    HashMap<String, String> resultp2 = new HashMap<String, String>();

  
    ImageLoaderprod imageLoader;

    Boolean har = false;

    int product_should_be_added = 0;

    public ListViewAdapter_prod_full(Context context,
                                     ArrayList<HashMap<String, String>> arraylistpassed) {
        this.context = context;
        data = arraylistpassed;
        imageLoader = new ImageLoaderprod(context);
        inflater = LayoutInflater.from(context);
        initialDisplayedValues = arraylistpassed;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        TextView prodid;
        TextView prodname;
        TextView prodprice;
        ImageView flag, tag_product_screen;
        TextView product_category;
        TextView product_offers;
        final ImageView wish_prsnt_img;
        final ImageView tag_cart_icon;
        TextView product_unit, offer_in_tag_product_list;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.listview_itemprod_vertical,
                parent, false);
        resultp = data.get(position);
        prodname = (TextView) itemView.findViewById(R.id.prodname);
        prodprice = (TextView) itemView.findViewById(R.id.prodprice);
        wish_prsnt_img = (ImageView) itemView.findViewById(R.id.wishlist_btn1);
        tag_cart_icon = (ImageView) itemView.findViewById(R.id.tag_cart_icon);
        product_category = (TextView) itemView
                .findViewById(R.id.product_category);
        product_offers = (TextView) itemView.findViewById(R.id.product_offers);
        product_unit = (TextView) itemView.findViewById(R.id.per_items);
        offer_in_tag_product_list = (TextView) itemView
                .findViewById(R.id.offer_in_tag_product_list);
        tag_product_screen = (ImageView) itemView
                .findViewById(R.id.tag_product_list);

        // Locate the ImageView in listview_item.xml
        flag = (ImageView) itemView.findViewById(R.id.flag);

        // Capture position and set results to the TextViews
        // prodid.setText(resultp.get(MainActivity.PRODID));
        prodname.setText(resultp.get(MainActivityprod.PRODNAME));
        prodprice.setText(resultp.get(MainActivityprod.PRODPRICE));
        product_category.setText(resultp.get(MainActivityprod.CATEGORY));
        if (resultp.get(MainActivityprod.OFFER).isEmpty()) {
            product_offers.setText("No Offers");
            tag_product_screen.setVisibility(View.INVISIBLE);
            offer_in_tag_product_list.setVisibility(View.INVISIBLE);
        } else {
            product_offers.setText(resultp.get(MainActivityprod.OFFER)
                    + " Available till "
                    + resultp.get(MainActivityprod.LASTDATE));
            offer_in_tag_product_list.setText(resultp
                    .get(MainActivityprod.OFFER));
        }

        product_unit.setText(resultp.get(MainActivityprod.UNIT));

        if (resultp.get(MainActivityprod.WISH_PRSNT).toString().equals("0")) {
            wish_prsnt_img.setImageResource(R.drawable.no_crl_mywish_icon);
            har = false;
        } else {
            wish_prsnt_img.setImageResource(R.drawable.hearticonnn);
            har = true;
        }

        imageLoader.DisplayImage(resultp.get(MainActivityprod.FLAG), flag);

        tag_cart_icon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (R.drawable.cart_red_icon == tag_cart_icon.getId()) {
                    tag_cart_icon.setImageResource(R.drawable.no_crl_cart_icon);
                } else {
                    tag_cart_icon.setImageResource(R.drawable.cart_red_icon);
                }

                resultp2 = data.get(position);
                prodid1 = resultp2
                        .get(com.cognizant.iot.activity.CatalogActivity.PRODID);
                new DownloadJSONforProductVariantProductFullList().execute();

            }
        });

        wish_prsnt_img.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resultp2 = data.get(position);
                prodid1 = resultp2.get(MainActivityprod.PRODID);

                if (R.drawable.hearticonnn == wish_prsnt_img.getId()) {
                    wish_prsnt_img
                            .setImageResource(R.drawable.no_crl_mywish_icon);
                } else {
                    wish_prsnt_img.setImageResource(R.drawable.hearticonnn);
                }

                new DownloadJSONforAXADDwishlistProductFullList().execute();
            }
        });

        itemView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // Get the position
                resultp = data.get(position);
                Intent intent = new Intent(context,
                        Product_description_productscreen.class);
                // Pass all data prodid
                intent.putExtra("prodid", resultp.get(MainActivityprod.PRODID));
                // Pass all data prodname
                intent.putExtra("prodname",
                        resultp.get(MainActivityprod.PRODNAME));
                // Pass all data prodprice
                intent.putExtra("prodprice",
                        resultp.get(MainActivityprod.PRODPRICE));
                // Pass all data flag
                intent.putExtra("flag", resultp.get(MainActivityprod.FLAG));
                intent.putExtra("desc", resultp.get(MainActivityprod.DESC));
                // intent.putExtra("beacon",
                // resultp.get(Product_Screen.BEACON));
                intent.putExtra("offer", resultp.get(MainActivityprod.OFFER));
                intent.putExtra("category",
                        resultp.get(MainActivityprod.CATEGORY));
                intent.putExtra("wish_present",
                        resultp.get(MainActivityprod.WISH_PRSNT));
                intent.putExtra("unit", resultp.get(MainActivityprod.UNIT));
                // Start SingleItemView Class
                context.startActivity(intent);
            }
        });
        return itemView;
    }
/*
      *      Call to get Product Variant for adding to cart
     */

    private class DownloadJSONforProductVariantProductFullList extends AsyncTask<Void, Void, Void> {
        String[] products = new String[1];
        String variantID = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            products[0] = prodid1;


        }

        @Override
        protected Void doInBackground(Void... params) {
            String productRequestUrl = String.format("https://landmarkdevret.cloudax.dynamics.com/Commerce/Products/Search?$top=250&api-version=7.1");
            Log.d("@@##", "productRequestUrl = " + productRequestUrl);
            ArrayList<HashMap<String, String>> arraylistprod = new ArrayList<HashMap<String, String>>();

            JSONObject jsonobject1detail = JSONFunctionsDetails_findprod
                    .getMultipleJSONfromURLAX(productRequestUrl, products, "");

            System.out.println("@@## VariantProductsResponseObject = " + jsonobject1detail.toString());
            try {

                JSONArray jsonarray = jsonobject1detail
                        .getJSONArray("value");

                for (int iir = 0; iir < jsonarray.length(); iir++) {
                    JSONObject jsonobjectAX = jsonarray
                            .getJSONObject(iir);


                    JSONObject compositionInfo = new JSONObject();

                    compositionInfo = jsonobjectAX.getJSONObject("CompositionInformation");
                    JSONObject variantInfo = new JSONObject();

                    variantInfo = compositionInfo.getJSONObject("VariantInformation");

                    JSONArray variants = new JSONArray();
                    variants = variantInfo.getJSONArray("Variants");

                    JSONObject insideVariantObj = new JSONObject();


                    // Loop kept just to iterate once as we are picking up the first variant ID
                    for (int jk = 0; jk < 1; jk++) {

                        insideVariantObj = variants
                                .getJSONObject(jk);


                        variantID = insideVariantObj
                                .getString("DistinctProductVariantId");
                    }


                }

            } catch (JSONException e) {

                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(Void args) {

            new DownloadJSONforaddCartProductFullList(variantID).execute();
        }
    }


    /*
     * add to cart
     */


    // DownloadJSON AsyncTask
    private class DownloadJSONforaddCartProductFullList extends AsyncTask<Void, Void, Void> {
        String str4 = null, str3 = null, str5 = null;

        String variantID = "";

        DownloadJSONforaddCartProductFullList(String variantid) {
            this.variantID = variantid;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(Void... params) {

			/*
             * System.setProperty("https.proxyHost", "10.243.115.76");
			 * System.setProperty("https.proxyPort", "6050");
			 * System.setProperty("http.proxyHost", "10.243.115.76");
			 * System.setProperty("http.proxyPort", "6050");
			 */
//            Log.d("@@##", "itemid ## = " + itemid + ", prodid1 = " + prodid1);
            // Create an array
            arraylist = new ArrayList<HashMap<String, String>>();
            // Retrieve JSON Objects from the given URL address
            jsonobject = JsonFuntionCart
                    .getJSONfromURL(
                            "http://lmcustomervalidationax.azurewebsites.net/api/cart/CreateCart?idToken=" + AccountState.getTokenID(),
                            variantID, "1");

            System.out.println("@@## ADD CART AX " + jsonobject);

            try {

                String message = jsonobject.getString("Id");

            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            Toast.makeText(
                    context,
                    "Request processed successfully\nProduct "
                            + " added to the Cart", Toast.LENGTH_LONG).show();

        }
    }


    /*
        WIshlist latest
     */
    // DownloadJSON AsyncTask
    private class DownloadJSONforAXADDwishlistProductFullList extends
            AsyncTask<Void, Void, JSONObject> {
        String str4 = null, str3 = null, str5 = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONObject doInBackground(Void... params) {
            /*
             * System.setProperty("https.proxyHost", "10.243.115.76");
			 * System.setProperty("https.proxyPort", "6050");
			 * System.setProperty("http.proxyHost", "10.243.115.76");
			 * System.setProperty("http.proxyPort", "6050");
			 */

            // Create an array
            arraylist = new ArrayList<HashMap<String, String>>();
            // Retrieve JSON Objects from the given URL address
//            jsonobject = JSONfunctions_wish_deleteproduct
//                    .getJSONfromURLAXADDwishlist(
//                            "https://landmarkdevret.cloudax.dynamics.com/data/tests",
//                            prodid1, itemid);


            jsonobject = JSONfunctions_wish_deleteproduct
                    .getJSONfromURLAXADDwishlist(
                            "http://lmcustomervalidationax.azurewebsites.net/api/WishList/CreateWishList?idtoken=" + AccountState.getTokenID(),
                            prodid1, "");

            System.out.println("@@## REsponse addwishlist \n"
                    + jsonobject);

            return jsonobject;
        }

        @Override
        protected void onPostExecute(JSONObject jsonobject) {

            try {
                if (jsonobject.has("Data")) {
                    JSONObject dataObject = jsonobject.getJSONObject("Data");
                    if (dataObject.getBoolean("success")) {
                        Toast.makeText(context, "Item has been added to your Wishlist",
                                Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(context, "Unable to update Wishlist, please try again",
                                Toast.LENGTH_LONG).show();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}