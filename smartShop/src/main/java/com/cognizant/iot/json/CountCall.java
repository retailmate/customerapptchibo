package com.cognizant.iot.json;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.iot.estimote.NotifyDemoActivity;
import com.cognizant.iot.model.AssetModel;
import com.cognizant.iot.model.OfferModel;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.retailmate.R;

public class CountCall extends AsyncTask<String, Void, String> {
    Context mContext;
    ArrayList<OfferModel> offerList = new ArrayList<OfferModel>();
    ArrayList<String> itemList = new ArrayList<String>();
    String assetID, jsonUrl;
    JSONObject jresponse = null;
    CountHttpCall countcall = new CountHttpCall();
    private int mProgressStatus = 0;

    ProgressBar progress;

    String adr, bID;

    TextView shelf_timer;

    TextView startTextView;
    TextView endTextView;

    AssetModel assetModel = new AssetModel();

    public CountCall(String arg, Context context, String imei, String beaconID, AssetModel assetModel) {
        // TODO Auto-generated constructor stub
        mContext = context;
        assetID = arg;
        adr = imei;
        bID = beaconID;
        this.assetModel = assetModel;

        progress = (ProgressBar) ((Activity) mContext)
                .findViewById(R.id.progressBar1);
    }


    @Override
    protected void onPreExecute() {
        // Extract all assets and overwrite existing files if debug build

        progress.setProgress(mProgressStatus);

    }

    @Override
    protected String doInBackground(String... arg0) {


//		jsonUrl = "https://landmarkdevret.cloudax.dynamics.com/data/BeaconOffers?$filter=beaconid";
        jsonUrl = "http://rmlmapi.azurewebsites.net/api/BeaconServices/GetBeaconBrandsAPI";

        //			String encodedurl = URLEncoder.encode(" eq " + "\'" + bID + "\'",
//					HTTP.UTF_8);
        try {

            if (AccountState.getOfflineMode()) {

            } else {
                jresponse = countcall.getJSONfromURLBRandProduct(jsonUrl, bID);

                System.out.println("@@## BRAND CALL RESPONSE " + jresponse);

            }
            progress.setProgress(50);

            return jresponse.toString();
        } catch (NullPointerException | ArrayIndexOutOfBoundsException e) {

        }
        return null;

    }

    @Override
    protected void onPostExecute(String doc) {


        JSONObject obj = null;
        JSONArray m_jArry = null;
        JSONArray w_jArry = null;

        try {
            if (AccountState.getOfflineMode()) {
                obj = new JSONObject(getbeaconbrandsResponse());
            } else {

                obj = new JSONObject(doc);
            }
            m_jArry = obj.getJSONArray("value");

            System.out.println("JSON array RECIEVED" + m_jArry);

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside;
                OfferModel am = new OfferModel();
                try {
                    jo_inside = m_jArry.getJSONObject(i);
//					am.setBeaconid(jo_inside.getString("beaconid"));
//					am.setCode(jo_inside.getString("PromotionCode"));
                    am.setName(jo_inside.getString("OfferDescription"));
                    am.setStart(jo_inside.getString("ValidFrom"));
                    am.setEnd(jo_inside.getString("ValidTo"));
                    am.setBrand(jo_inside.getString("BrandName"));

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                // Add your values in your `ArrayList` as below:

                offerList.add(am);
                // Same way for other value...
            }
        } catch (JSONException | NullPointerException | ArrayIndexOutOfBoundsException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        TextView statusTextView = (TextView) ((Activity) mContext)
                .findViewById(R.id.code);
        startTextView = (TextView) ((Activity) mContext)
                .findViewById(R.id.startdate_shelf);
        endTextView = (TextView) ((Activity) mContext)
                .findViewById(R.id.enddate_shelf);

        statusTextView.setText("CODE: " + assetModel.getOffers());


        List<String> blist = new ArrayList<>();

        for (int i = 0; i < offerList.size(); i++) {
            blist.add(offerList.get(i).getBrand());
        }

        NotifyDemoActivity.brandList = blist;

//        System.out.println("@@## brandList " + NotifyDemoActivity.brandList.get(0));


        final Spinner spinner = (Spinner) ((Activity) mContext).findViewById(R.id.spinner1);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext,
                R.layout.spinner, NotifyDemoActivity.brandList);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                Toast.makeText(mContext, "CLicked" + position,
                        Toast.LENGTH_SHORT);
                System.out.println("Clicked");

                BrandCall bCallobj = new BrandCall("empty", mContext,
                        "", String.valueOf(spinner.getSelectedItem()), assetModel.getAssetBeaconId());
                bCallobj.execute();


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });


        shelf_timer = (TextView) ((Activity) mContext)
                .findViewById(R.id.shelf_timer);
        Date date = null;
        Date sdate = null;

        try {
            double current_time = System.currentTimeMillis();
            String endDate = offerList.get(0).getEnd();
            String startDate = offerList.get(0).getStart();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            date = sdf.parse(endDate);
            sdate = sdf.parse(startDate);

            SimpleDateFormat sdfnew1 = new SimpleDateFormat(
                    "dd MMM, yyyy h:mm a");
            String dateString1 = sdfnew1.format(sdate);
            System.out.println(dateString1);

            startTextView.setText(dateString1);

            SimpleDateFormat sdfnew = new SimpleDateFormat(
                    "dd MMM, yyyy h:mm a");
            String dateString = sdfnew.format(date);
            endTextView.setText(dateString);


            long howMany = (date.getTime() - System.currentTimeMillis());
            new CountDownTimer(howMany, 1000) {

                public void onTick(long millisUntilFinished) {
                    long millis = millisUntilFinished;
                    String hms = String.format(
                            "%02d:%02d:%02d",
                            TimeUnit.MILLISECONDS.toHours(millis),
                            TimeUnit.MILLISECONDS.toMinutes(millis)
                                    - TimeUnit.HOURS
                                    .toMinutes(TimeUnit.MILLISECONDS
                                            .toHours(millis)),
                            TimeUnit.MILLISECONDS.toSeconds(millis)
                                    - TimeUnit.MINUTES
                                    .toSeconds(TimeUnit.MILLISECONDS
                                            .toMinutes(millis)));
                    System.out.println(hms);

                    shelf_timer.setText(hms);
                }

                public void onFinish() {
                    shelf_timer.setText("Offer Expired!");
                }
            }.start();
        } catch (IndexOutOfBoundsException | NullPointerException | ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    /*
    OFFLINE FUNCTION FOR BEACONS
     */


    public String getbeaconbrandsResponse() {
        String json = null;
        try {
            InputStream is = mContext.getAssets().open("beaconbrandsoffline.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

}