package com.cognizant.iot.json;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

import com.cognizant.iot.activity.CatalogActivity;
import com.cognizant.iot.activity.Login_Screen;

import com.cognizant.iot.model.AssetModel;


//DownloadJSON AsyncTask
public class Logout extends AsyncTask<String, Void, String> {

	Context mContext;
	ArrayList<AssetModel> assetList = new ArrayList<AssetModel>();
	ArrayList<String> wishList = new ArrayList<String>();
	String assetID, jsonUrl;
	String jresponse = null;
	JSONParser jsonparser = new JSONParser();
	String adr;
	static InputStream is = null;
	static JSONObject jobj = null;
	static String jsondata = "";

	private ProgressDialog mProgressDialog;

	public Logout(String string, Context context, String imei) {
		// TODO Auto-generated constructor stub
		mContext = context;
		adr = imei;

		mProgressDialog = new ProgressDialog(context);

	}

	@Override
	protected void onPreExecute() {

		mProgressDialog.setMessage("Please wait..");

		mProgressDialog.setIndeterminate(true);
		mProgressDialog.show();
		super.onPreExecute();

	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub

		jsonUrl = CatalogActivity.urlPart + "/api/UserManagement/Logout";
		System.out.println("JSON URL:" + jsonUrl);
		try {
			jresponse = makeHttpRequest(jsonUrl, adr);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jresponse;
	}

	@Override
	protected void onPostExecute(String doc) {
		// TODO Auto-generated method stub

		mProgressDialog.dismiss();

		JSONObject obj = null;
		try {
			obj = new JSONObject(doc);

			JSONObject obj1 = new JSONObject(obj.getString("Response"));
			JSONObject obj2 = new JSONObject(obj1.getString("header"));
			String status = obj2.getString("message");

			if (status.equals("Request processed")) {

				Toast.makeText(mContext, "You have Logged out!",
						Toast.LENGTH_LONG).show();

				Intent intent = new Intent(mContext, Login_Screen.class);
				mContext.startActivity(intent);
				((Activity) mContext).finish();

			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		super.onPostExecute(doc);
	}

	public String makeHttpRequest(String url, String adr2)
			throws UnsupportedEncodingException {
		DefaultHttpClient httpclient = new DefaultHttpClient();
		trustEveryone();
		System.out.println("@@## URL in post call" + url);
		HttpPost httpPost = new HttpPost(url);

		/*
		 * Used for conenction to ERP
		 */
		// httpPost.setHeader("User-Agent", "SuiteScript-Call");
		// httpPost.setHeader(
		// "Authorization",
		// "NLAuth nlauth_account=TSTDRV1174894, nlauth_email=Harisankar.B@cognizant.com,nlauth_signature=Cognizant@123, nlauth_role=3");
		httpPost.setHeader("Content-Type", "application/json");

		String adr, bID;

		adr = adr2;

		// adr = "000000000007854";
		// bID = "DF:AF:A9:FD:F8:6D";
		// HttpEntity entity = new StringEntity(
		// "{\"macaddr\":\"000000000007854\",\"beacon\":\"DF:AF:A9:FD:F8:6D\"}");

		HttpEntity entity = new StringEntity("{\"macaddr\":" + "\"" + adr
				+ "\"" + "}");

		httpPost.setEntity(entity);

		try {
			HttpResponse httpresponse = httpclient.execute(httpPost);
			System.out.println("@@@@@@@@@@@@@@@@@@@@"
					+ httpresponse.getStatusLine().toString());

			HttpEntity httpentity = httpresponse.getEntity();
			is = httpentity.getContent();

			System.out.println("@@### " + is.toString());

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			return "wrong";
		}

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			try {
				while ((line = reader.readLine()) != null) {
					sb.append(line + "\n");

				}
				is.close();
				jsondata = sb.toString();

				System.out.println("@@##JSON DATA" + jsondata);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jsondata;

	}

	public static void trustEveryone() {
		try {
			HttpsURLConnection
					.setDefaultHostnameVerifier(new HostnameVerifier() {

						@Override
						public boolean verify(String hostname,
								SSLSession session) {
							// TODO Auto-generated method stub
							return true;
						}
					});
			SSLContext context = SSLContext.getInstance("TLS");
			context.init(null, new X509TrustManager[] { new X509TrustManager() {
				public void checkClientTrusted(X509Certificate[] chain,
						String authType) throws CertificateException {
				}

				public void checkServerTrusted(X509Certificate[] chain,
						String authType) throws CertificateException {
				}

				public X509Certificate[] getAcceptedIssuers() {
					return new X509Certificate[0];
				}
			} }, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(context
					.getSocketFactory());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}