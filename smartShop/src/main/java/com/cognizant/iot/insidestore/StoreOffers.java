package com.cognizant.iot.insidestore;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.cognizant.retailmate.R;
import com.google.gson.Gson;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by 452781 on 12/6/2016.
 */
public class StoreOffers extends AppCompatActivity {


    Gson gson;

    TextView assoName, assoID, assoCallTag;
    RelativeLayout associateDetails;

    RecyclerView offersRecyclerView;
    LinearLayoutManager linearLayoutManager;
    InsideStoreOffersAdapter offersAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        gson = new Gson();
        setContentView(R.layout.insidestore_offerslist);


        offersRecyclerView = (RecyclerView) findViewById(R.id.insidestoreoffers_recycler_view);
        linearLayoutManager = new LinearLayoutManager(StoreOffers.this);

        offersRecyclerView.setLayoutManager(linearLayoutManager);


        offersCalltoOneRetail();
    }


    public void offersCalltoOneRetail() {
        {


            String url = "http://oneretail.southcentralus.cloudapp.azure.com:8084/api/Promo?CID=%271%27&SID=%27s1%27&BID=%27%27&DID=%27%27";

            StringRequest req = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    InsideStoreOffersModel[] associateDetailsModel = gson.fromJson(response, InsideStoreOffersModel[].class);

                    ArrayList<InsideStoreOffersModel> offersarrayList = new ArrayList<InsideStoreOffersModel>(Arrays.asList(associateDetailsModel));

                    offersAdapter = new InsideStoreOffersAdapter(StoreOffers.this, offersarrayList);
                    offersRecyclerView.setAdapter(offersAdapter);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }) {


                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    int mStatusCode = response.statusCode;

                    Log.d("@@##", "Response Back " + response.statusCode);
                    if (mStatusCode == HttpURLConnection.HTTP_OK) {

                    } else {

                    }
                    return super.parseNetworkResponse(response);
                }

            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(req);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.aboutusmenu, menu);
        // setMenuBackground();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title

        // Handle action bar actions click
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
