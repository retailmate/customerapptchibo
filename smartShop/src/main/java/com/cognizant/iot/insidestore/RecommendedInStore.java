package com.cognizant.iot.insidestore;

import android.content.Context;
import android.os.AsyncTask;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.iot.activity.CatalogActivity;
import com.cognizant.iot.activityfindproduct.JSONFunctionsDetails_findprod;
import com.cognizant.iot.activityproductlist.ImageLoaderprod;
import com.cognizant.iot.activitywishlist.ImageLoader_wish;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.retailmate.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by 452781 on 12/6/2016.
 */
public class RecommendedInStore {

    /**
     * Created by 452781 on 9/22/2016.
     */

    Context context;


    ImageView btnDismiss, cartButon, wishButon;

    TextView recomProductName;
    ImageView recomProductImage;

    String name;
    String proID;

    JSONObject jsonobject;
    JSONArray jsonarray;

    ImageLoaderprod imageLoader;

    public RecommendedInStore(String proID, Context context) {

        this.context = context;
        this.proID = proID;
        imageLoader = new ImageLoaderprod(context);
        if (AccountState.getOfflineMode()) {

        } else {
            new GetRecommendedProductDetails(proID).execute();
        }
    }


    public void getPopUp(View parent, final Context context) {

        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.80);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View popUpView = inflater.inflate(R.layout.insidestorerecommendedproduct, null, false);
        final PopupWindow popup = new PopupWindow(popUpView, screenWidth,
                WindowManager.LayoutParams.WRAP_CONTENT, true);
        popup.setContentView(popUpView);
        btnDismiss = (ImageView) popUpView.
                findViewById(R.id.cancelButton);

        wishButon = (ImageView) popUpView.findViewById(R.id.wishButon);
        cartButon = (ImageView) popUpView.findViewById(R.id.cartButon);

        recomProductName = (TextView) popUpView.findViewById(R.id.recomProductName);
        recomProductImage = (ImageView) popUpView.findViewById(R.id.recomProductImage);


        popup.showAtLocation(parent, Gravity.CENTER_HORIZONTAL, 10, 10);

        if (AccountState.getOfflineMode()) {
            getRecommdedproductDetailsOffline();
        }

        wishButon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(context, "Added to wishlist", Toast.LENGTH_SHORT).show();
                popup.dismiss();

            }
        });

        cartButon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(context, "Added to cart", Toast.LENGTH_SHORT).show();
                popup.dismiss();

            }
        });
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                popup.dismiss();
            }
        });

    }

    private class GetRecommendedProductDetails extends
            AsyncTask<Void, Void, Void> {

        String productID, suggestedProductName, suggestionImageURL;

        public GetRecommendedProductDetails(String suggestionID) {
            // TODO Auto-generated constructor stub
            productID = suggestionID;
            System.out.println("@@## RECEIVED SUGGESTED ID " + productID);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        public void execute(String suggestionID) {
            // TODO Auto-generated method stub

        }

        @Override
        protected Void doInBackground(Void... params) {
            Log.i("UPDATE", "1");

            System.out.println("@@#$$PRODUCT ID OneretailRecommended product call:"
                    + productID);

            JSONObject jsonobject1 = JSONFunctionsDetails_findprod
                    .getJSONfromURLAX(
                            "https://landmarkdevret.cloudax.dynamics.com/Commerce/Products/Search?$top=250&api-version=7.1",
                            productID, CatalogActivity.imei);

            try {

                jsonarray = jsonobject1.getJSONArray("value");

                for (int i = 0; i < jsonarray.length(); i++) {
                    jsonobject = jsonarray.getJSONObject(i);

                    JSONObject insideImage = new JSONObject();
                    insideImage = jsonobject.getJSONObject("Image");
                    System.out.println("@@## Oneretail Recommended product image:"
                            + insideImage);

                    JSONArray insideitemimage = new JSONArray();
                    insideitemimage = insideImage.getJSONArray("Items");


                    JSONObject insideImageURL = new JSONObject();
                    for (int j = 0; j < insideitemimage.length(); j++) {

                        insideImageURL = insideitemimage.getJSONObject(i);
                    }

                    suggestedProductName = jsonobject.getString("ProductName");
                    System.out.println("@@#$$One retail Recommended product name:"
                            + suggestedProductName);


                    suggestionImageURL = "https://landmarkdevret.cloudax.dynamics.com/MediaServer/"
                            + insideImageURL.getString("Url");

                }

            } catch (JSONException e) {

                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }

            Log.i("UPDATE", "2");
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            recomProductName.setText(suggestedProductName);

            imageLoader
                    .DisplayImage(suggestionImageURL, recomProductImage);


            super.onPostExecute(aVoid);

        }
    }

    /*
    OFFLINE FUNCTION
     */
    private void getRecommdedproductDetailsOffline() {

        if (proID.equals("68719483873")) {
            recomProductName.setText("Sitting British Bulldog Figurine- Reds");

            recomProductImage.setImageResource(R.drawable.product5);

        } else if (proID.equals("68719483124")) {
            recomProductName.setText("Blue and White 16 Piece Dinnerware Set");
            recomProductImage.setImageResource(R.drawable.product3);

        }
    }

}
