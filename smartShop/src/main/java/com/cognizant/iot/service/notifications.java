package com.cognizant.iot.service;

import android.app.IntentService;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.cognizant.iot.geolocation.ProximityReciever;

public class notifications extends IntentService implements LocationListener {

    LocationManager lm;
    BroadcastReceiver proximityReciever;
    double lat = 12.8247964, long1 = 80.2208028;
    float radius = (float) 0.1;

    // Intent Action
    String ACTION_FILTER = "com.example.proximityalert";

    Location location;
    double latitude;
    double longitude;
    // flag for GPS status
    boolean isGPSEnabled = false;

    // flag for network status
    boolean isNetworkEnabled = false;

    // flag for GPS status
    boolean canGetLocation = false;

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 1; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 10 * 1 * 1; // 1 minute

    // END

    public notifications() {
        super("MyService");

        System.out.println("@@#@ service started");

        // TODO Auto-generated method stub

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onLocationChanged(Location newLocation) {
        // TODO Auto-generated method stub
        Location old = new Location("OLD");
        old.setLatitude(lat);
        old.setLongitude(long1);

        double distance = newLocation.distanceTo(old);

        Log.i("MyTag", "Distance: " + distance);
    }

    // NEW

    /**
     * Function to get latitude
     */
    public double getLatitude() {
        if (location != null) {
            latitude = location.getLatitude();
            Log.d("@@##", "latitude: " + latitude);
        }

        // return latitude
        return latitude;
    }

    /**
     * Function to get longitude
     */
    public double getLongitude() {
        if (location != null) {
            longitude = location.getLongitude();
        }

        // return longitude
        return longitude;
    }

    // END
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onProviderEnabled(String provider) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onProviderDisabled(String provider) {
        // TODO Auto-generated method stub

    }

    public Location getLocation() {
        try {

            // getting GPS status
            isGPSEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = lm
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
            } else {
                this.canGetLocation = true;
                if (isNetworkEnabled) {
                    lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("Network", "Network");
                    if (lm != null) {
                        location = lm
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        Log.d("GPS Enabled", "GPS Enabled");
                        if (lm != null) {
                            location = lm
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        // TODO Auto-generated method stub
        // i'm registering my Receiver First
        proximityReciever = new ProximityReciever();
        registerReceiver(proximityReciever, new IntentFilter(
                ACTION_FILTER));

        // i'm calling ther service Location Manager
        lm = (LocationManager) getSystemService(LOCATION_SERVICE);

        // for debugging...
        // lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 10,
        // this);
        // New
        getLocation();
        // End
        // Setting up My Broadcast Intent
        Intent i = new Intent(ACTION_FILTER);
        PendingIntent pi = PendingIntent.getBroadcast(getApplicationContext(),
                -1, i, 0);

        // setting up proximituMethod
        lm.addProximityAlert(lat, long1, radius, -1, pi);

        // Toast.makeText(
        // getApplicationContext(),
        // "Your Location is - \nLat: " + latitude + "\nLong: "
        // + longitude, Toast.LENGTH_LONG).show();
        // END
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        if (proximityReciever != null) {
            unregisterReceiver(proximityReciever);
        }
        super.onDestroy();

    }

}