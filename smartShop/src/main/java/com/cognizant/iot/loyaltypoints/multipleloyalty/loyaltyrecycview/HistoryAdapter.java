package com.cognizant.iot.loyaltypoints.multipleloyalty.loyaltyrecycview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.cognizant.iot.loyaltypoints.multipleloyalty.loyaltymodel.LoyaltyResponseModel;
import com.cognizant.retailmate.R;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by 543898 on 12/29/2016.
 */

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder1> {

    private static final int REDEEM = 0;
    private static final int ACCURAL = 1;
    LoyaltyResponseModel loyaltyResponseModelList;
    Context context1;

    public HistoryAdapter(Context context2, LoyaltyResponseModel values2) {

        loyaltyResponseModelList = values2;

        context1 = context2;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View v) {
            super(v);
        }
    }

    public static class ViewHolder1 extends ViewHolder {

        public TextView pointsView, typeView, dateView;

        public ImageView loyaltyTypeImage;

        public ViewHolder1(View v) {

            super(v);

            pointsView = (TextView) v.findViewById(R.id.points);

            typeView = (TextView) v.findViewById(R.id.type);

            dateView = (TextView) v.findViewById(R.id.date);

            loyaltyTypeImage = (ImageView) v.findViewById(R.id.loyalty_type_img);

        }
    }


    @Override
    public int getItemViewType(int position) {
        String transactionType = loyaltyResponseModelList.getTransactionDetail().get(position).getTransactiontype();
        if (transactionType.equalsIgnoreCase("Accrual")) {
            return ACCURAL;
        } else {
            return REDEEM;
        }
    }

    @Override
    public ViewHolder1 onCreateViewHolder(ViewGroup parent, int viewType) {

        Log.e("****", "^^^^" + viewType);
        View v;

        if (viewType == REDEEM) {
            v = LayoutInflater.from(context1).inflate(R.layout.loyalty_multiple_history_recycler_view_item, parent, false);

            return new ViewHolder1(v);
        } else {
            v = LayoutInflater.from(context1).inflate(R.layout.loyalty_multiple_history_layout_accural, parent, false);
            return new ViewHolder1(v);
        }

        /*String transactionType = loyaltyResponseModelList.getTransactionDetail().get(position).getTransactiontype();

        if (transactionType.equalsIgnoreCase("Accrual")) {
            Vholder.pointsView.setText(loyaltyResponseModelList.getTransactionDetail().get(position).getAccrualPoints());
            Vholder.loyaltyTypeImage.setImageResource(R.drawable.accural);
        } else if (transactionType.equalsIgnoreCase("Redemption")) {
            Vholder.pointsView.setText(loyaltyResponseModelList.getTransactionDetail().get(position).getRedemptionPoints());
            Vholder.loyaltyTypeImage.setImageResource(R.drawable.redeem);
        }
*/
    }

    @Override
    public void onBindViewHolder(ViewHolder1 Vholder, int position) {
        String transactionType = loyaltyResponseModelList.getTransactionDetail().get(position).getTransactiontype();

        if (transactionType.equalsIgnoreCase("Accrual")) {
            Vholder.pointsView.setText(loyaltyResponseModelList.getTransactionDetail().get(position).getAccrualPoints());
            Vholder.loyaltyTypeImage.setImageResource(R.drawable.accural);
        } else if (transactionType.equalsIgnoreCase("Redemption")) {
            Vholder.pointsView.setText(loyaltyResponseModelList.getTransactionDetail().get(position).getRedemptionPoints());
            Vholder.loyaltyTypeImage.setImageResource(R.drawable.redeem);
        }

//        Vholder.typeView.setText(transactionType);

        String strDateTimeBoj = loyaltyResponseModelList.getTransactionDetail().get(position).getTransactionDate();
        // first you need to use proper date formatter
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        try {
            Date date = df.parse(strDateTimeBoj);
            System.out.println(df.format(date));

            String minutes;
            if (date.getMinutes() < 10)
                minutes = "0" + date.getMinutes();
            else
                minutes = String.valueOf(date.getMinutes());
            Vholder.dateView.setText((date.getDate() + "/"
                    + (date.getMonth() + 1) + "/" + (date.getYear() + 1900)
                    + " | " + date.getHours() + ":" + minutes)
                    .toString());

            // datetimeLoyalty.setText(date.toString().substring(0,
            // date.toString().indexOf("G")));

        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {

        return loyaltyResponseModelList.getTransactionDetail().size();
    }
}