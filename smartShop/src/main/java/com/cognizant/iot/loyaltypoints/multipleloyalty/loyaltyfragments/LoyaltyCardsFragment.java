package com.cognizant.iot.loyaltypoints.multipleloyalty.loyaltyfragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cognizant.iot.loyaltypoints.multipleloyalty.loyaltyrecycview.LoyaltyCards;
import com.cognizant.iot.loyaltypoints.multipleloyalty.loyaltyrecycview.RecyclerViewAdapter;
import com.cognizant.retailmate.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 543898 on 1/23/2017.
 */

public class LoyaltyCardsFragment extends Fragment {

    List<LoyaltyCards> loyaltyCardses = new ArrayList<>();

    int noOfGrids = 2;

    RecyclerView recyclerView;

    RecyclerViewAdapter recyclerView_Adapter;

    RecyclerView.LayoutManager recyclerViewLayoutManager;

    String[] numbers = {
            "374 points",
            "974 points",
//            "250 points",
//            "355 points",
//            "604 points",
//            "819 points",
//            "203 points",
//            "484 points",
//            "1,140 points"
    };

    Integer[] brandImage = {
            R.drawable.cognizant_retailmate_logo,
            R.drawable.logo_tchibo,
//            R.drawable.landmark_rewards,
//            R.drawable.armani_new,
//            R.drawable.stevemadden_new,
//            R.drawable.nike_new,
//            R.drawable.elc_new,
//            R.drawable.starbucks_new,
//            R.drawable.etihad_new
    };

    public LoyaltyCardsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View cardLayout = inflater.inflate(R.layout.loyalty_multiple_loyalty_cards_fragment, container, false);

        for (int i = 0; i < numbers.length; i++) {
            LoyaltyCards loyaltyCards = new LoyaltyCards();
            loyaltyCards.setRewards(numbers[i]);
            loyaltyCards.setRewardsBrand(brandImage[i]);
            loyaltyCardses.add(loyaltyCards);
        }

        recyclerView = (RecyclerView) cardLayout.findViewById(R.id.recycler_view1);

        //Change 2 to your choice because here 2 is the number of Grid layout Columns in each row.
        recyclerViewLayoutManager = new GridLayoutManager(cardLayout.getContext(), noOfGrids);

        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(recyclerViewLayoutManager);

        recyclerView_Adapter = new RecyclerViewAdapter(cardLayout.getContext(), loyaltyCardses);

        recyclerView.setAdapter(recyclerView_Adapter);

        return cardLayout;

    }

}