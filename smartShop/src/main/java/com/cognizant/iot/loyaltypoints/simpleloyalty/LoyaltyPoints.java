package com.cognizant.iot.loyaltypoints.simpleloyalty;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cognizant.iot.mycart.MyCart;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.iot.utils.GlobalClass;
import com.cognizant.retailmate.R;

public class LoyaltyPoints extends AppCompatActivity {

    ProgressBar loyaltyProgress;
    Button redeemPoints;

    Boolean FromCart;
    int pointsForLoyalty = 200;

    ProgressDialog mProgressDialog;
    ArrayList<HashMap<String, String>> arraylist;
    JSONObject jsonobject, jo;
    ListView listview;
    JSONArray jsonarray;
    LoyaltyPointsAdapter adapter2;

    String loyaltyPoints;
    TextView loyaltyPointsTV;

    GlobalClass mApplication;

    Boolean islp = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_loyalty);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarLoyalty);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        // getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        // getActionBar().setCustomView(R.layout.actionbar_inside);
//		getActionBar().setHomeAsUpIndicator(R.drawable.back_aro);
//		getActionBar().setDisplayHomeAsUpEnabled(true);
//		getActionBar().setHomeButtonEnabled(true);
//		getActionBar().setDisplayShowTitleEnabled(true);
//		getActionBar().setBackgroundDrawable(
//				new ColorDrawable(Color.parseColor("#B5D000")));
//		getActionBar().setIcon(
//				new ColorDrawable(getResources().getColor(
//						android.R.color.transparent)));
//
//		int titleId = getResources().getIdentifier("action_bar_title", "id",
//				"android");
//
//		TextView abTitle = (TextView) findViewById(titleId);
//		abTitle.setTextColor(Color.parseColor("#ffffff"));

        mApplication = (GlobalClass) getApplicationContext();
        /*
         * For lotyalty dummy call
		 */

        islp = mApplication.isLp();

		/*
         *
		 */

        redeemPoints = (Button) findViewById(R.id.redeemPoints);
        loyaltyPointsTV = (TextView) findViewById(R.id.loyaltyPoints);

        loyaltyProgress = (ProgressBar) findViewById(R.id.loyaltyProgress);

        FromCart = getIntent().getBooleanExtra("FromCart", false);

        redeemPoints.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (!FromCart) {
                    Intent intent = new Intent(getApplicationContext(),
                            MyCart.class);
                    intent.putExtra("whichClass", "loyalty");
                    intent.putExtra("loyaltyPoints", pointsForLoyalty);
                    startActivity(intent);
                    finish();
                } else {

                    MyCart.loyaltyRedeem.setVisibility(View.INVISIBLE);
                    MyCart.loyaltyPointsNumber.setVisibility(View.VISIBLE);
                    MyCart.loyaltyPointsNumber.setText(String.valueOf(
                            pointsForLoyalty).toString());

                    if (Double.parseDouble((String) MyCart.loyaltyPointsNumber
                            .getText()) > Double
                            .parseDouble((String) MyCart.totalValueCart
                                    .getText())) {
                        MyCart.loyaltyPointsNumber
                                .setText(MyCart.totalValueCart.getText()
                                        .toString());
                    }

                    finish();
                }

            }
        });

        // Execute DownloadJSON AsyncTask
        new DownloadJSON().execute();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title

        // Handle action bar actions click
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // DownloadJSON AsyncTask
    private class DownloadJSON extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
            // mProgressDialog = new ProgressDialog(LoyaltyPoints.this);
            // // Set progressdialog title
            // mProgressDialog.setTitle("Getting Products for the selected Order");
            // // Set progressdialog message
            // mProgressDialog.setMessage("Loading...");
            // mProgressDialog.setIndeterminate(false);
            // // Show progressdialog
            // mProgressDialog.show();

            loyaltyProgress.setProgress(25);
        }

        @Override
        protected Void doInBackground(Void... params) {

            // Create an array
            arraylist = new ArrayList<HashMap<String, String>>();
            // Retrieve JSON Objects from the given URL address
            // jsonobject = Jsonfunctions_loyalty
            // .getJSONfromURL(CatalogActivity.urlPart
            // + "/api/Order/GetOrderDetails");

            if (AccountState.getOfflineMode()) {
                try {
                    jsonobject = new JSONObject(getloyaltyResponse());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                jsonobject = Jsonfunctions_loyalty
                        .getJSONfromURL("http://rmlmapi.azurewebsites.net/api/MSCRM/Customer/GetPointsHistory");
            }

            try {

                loyaltyPoints = jsonobject.getString("ReedemptionPoint");
                //
                // jo = jsonobject.getJSONObject("Transaction");

                // Locate the array name in JSON
                jsonarray = jsonobject.getJSONArray("TransactionDetail");

                for (int i = 0; i < jsonarray.length(); i++) {
                    HashMap<String, String> map = new HashMap<String, String>();
                    jsonobject = jsonarray.getJSONObject(i);
                    // Retrive JSON Objects
                    map.put("type", jsonobject.getString("Transactiontype"));
                    if (jsonobject.getString("Transactiontype").equals(
                            "Redemption")) {
                        map.put("points",
                                jsonobject.getString("RedemptionPoints"));
                    } else {
                        map.put("points", jsonobject.getString("AccrualPoints"));
                    }
                    map.put("date", jsonobject.getString("TransactionDate"));
                    arraylist.add(map);
                }
            } catch (NullPointerException | JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }

            loyaltyProgress.setProgress(50);
            return null;

        }

        @Override
        protected void onPostExecute(Void args) {

            // Toast.makeText(getApplicationContext(), jsonobject.toString(),
            // Toast.LENGTH_LONG).show();

			/*
             * dummy part for loaylty redeem
			 */

            System.out.println("CHECKING FOR DUMMY LOYALTY " + islp);

            loyaltyPointsTV.setText(loyaltyPoints);
            if (islp) {
                HashMap<String, String> map = new HashMap<String, String>();
                // Retrive JSON Objects
                map.put("type", "Redemption");

                map.put("points", String.valueOf(mApplication.getLpRedeemed()));
                map.put("date",
                        new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
                                .format(Calendar.getInstance().getTime()));
                arraylist.add(map);

                System.out.println("LOYALTYPOINTS arraylist "
                        + arraylist.get(arraylist.size() - 1).get("type"));

                loyaltyPointsTV.setText(String.valueOf((Double
                        .parseDouble(loyaltyPoints) - mApplication
                        .getLpRedeemed())));
            }

            // Locate the listview in listview_main.xml
            listview = (ListView) findViewById(R.id.loyalityHistorylist);
            // Pass the results into ListViewAdapter.java
            adapter2 = new LoyaltyPointsAdapter(LoyaltyPoints.this, arraylist);
            // Set the adapter to the ListView
            listview.setAdapter(adapter2);
            // Close the progressdialog

            pointsForLoyalty = Integer.parseInt(loyaltyPoints);
            loyaltyProgress.setProgress(100);
            loyaltyProgress.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();

        mApplication.setLp(false);
        mApplication.setLpRedeemed(0);
    }



    /*
    Offline mode
     */


    public String getloyaltyResponse() {
        String json = null;
        try {
            InputStream is = getAssets().open("loyaltyoffline.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

}
