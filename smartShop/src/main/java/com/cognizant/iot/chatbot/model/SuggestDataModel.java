package com.cognizant.iot.chatbot.model;

/**
 * Created by 540472 on 12/22/2016.
 */
public class SuggestDataModel {

    public String getSuggestion() {
        return suggestion;
    }

    public void setSuggestion(String suggestion) {
        this.suggestion = suggestion;
    }

    String suggestion;

}
