package com.cognizant.iot.chatbot.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cognizant.iot.chatbot.model.SuggestDataModel;
import com.cognizant.retailmate.R;

import java.util.ArrayList;

/**
 * Created by 540472 on 12/22/2016.
 */

public class SuggestionListAdapter extends RecyclerView.Adapter<SuggestionListAdapter.ProductHolder> {
    private static final String CustomTag = "CustomTag";

    private ArrayList<SuggestDataModel> suggetsionList;
    private Context mContext;

    public SuggestionListAdapter(Context context, ArrayList<SuggestDataModel> suggetsionList) {
        this.suggetsionList = suggetsionList;
        this.mContext = context;
        Log.e(CustomTag,"#### SuggestionListAdapter");
    }

    @Override
    public ProductHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chatbot_suggestion, null);
        ProductHolder mh = new ProductHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(ProductHolder holder, final int i) {
        System.out.println("#### onBindViewHolder" );
        final SuggestDataModel suggestData = suggetsionList.get(i);
        System.out.println("#### suggestData.getSuggestion()" + suggestData.getSuggestion());
        holder.suggestionText.setText(suggestData.getSuggestion());
        holder.suggestLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Log.e(CustomTag,"onClick viewHolder");

                LocalBroadcastManager localBroadcastManager = LocalBroadcastManager
                        .getInstance(mContext);
                Intent intent = new Intent("suggestion.click");
                intent.putExtra("reply",suggestData.getSuggestion());
                mContext.sendBroadcast(intent);
                localBroadcastManager.sendBroadcast(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return suggetsionList.size();
    }

    public class ProductHolder extends RecyclerView.ViewHolder {

        protected TextView suggestionText;
        protected LinearLayout suggestLayout;


        public ProductHolder(final View view) {
            super(view);

            this.suggestionText = (TextView) view.findViewById(R.id.suggestion);
            this.suggestLayout = (LinearLayout) view.findViewById(R.id.suggestion_linear_layout);


        }

    }

}