package com.cognizant.iot.chatbot;

import android.os.AsyncTask;
import android.util.Log;

import com.cognizant.retailmate.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by 540472 on 12/27/2016.
 */
public class CartAsyncTask extends AsyncTask<String,Void,String> {

    private static final String CustomTag="CustomTag";
    ChatUtil util=new ChatUtil();

    @Override
    protected String doInBackground(String... params) {
        Log.d(CustomTag,"in CartAsyncTask");
        BaseConnection con = new BaseConnection();
        if (ChatGlobal.isaddToCartAPI){
            ChatGlobal.isaddToCartAPI=false;
            con.isAddApi=true;
        }
        String jsonString="server not working";
        try {
            Log.d(CustomTag,"CartAsyncTask ----->>>> in try");
            jsonString = con.run(params[0]);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonString;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        Log.d(CustomTag, "in CartAsyncTask---------->>>>>  " + s);
        if (s.equalsIgnoreCase("server not working")) {
            util.setReplyMessage(ChatGlobal.mContext.getResources().getString(R.string.bot_try_later_reply));

        } else {
            String apiResponse = s;

            //Getting Response from product list Api

            try {
                JSONObject obj = new JSONObject(apiResponse);
                JSONObject dataObj =obj.getJSONObject("Data");
                JSONObject cartDataObj =dataObj.getJSONObject("CartData");
                JSONArray cartArray = cartDataObj.getJSONArray("CartLines");
                Log.d(CustomTag,"cartArray.length = "+cartArray.length());
                for (int i=0;i<cartArray.length();i++) {
                    JSONObject itemObj = cartArray.getJSONObject(i);
                    String ProductId = itemObj.getString("ProductId");
                    Log.d(CustomTag,"productId = "+ProductId);
                }
                /**
                 * edit here
                 * */

                Log.d(CustomTag,"Amount Due = "+cartDataObj.getString("AmountDue"));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
