package com.cognizant.iot.chatbot;

import android.os.AsyncTask;
import android.util.Log;

import com.cognizant.retailmate.R;

import java.io.IOException;

/**
 * Created by 540472 on 1/25/2017.
 * Called only when add to cart requires variant Ids.
 */
public class VariantAsyncTask  extends AsyncTask<String,Void,String> {
    private static final String CustomTag="CustomTag";
    ChatUtil util=new ChatUtil();

    @Override
    protected String doInBackground(String... params) {
        Log.d(CustomTag,"in CartAsyncTask");
        BaseConnection con = new BaseConnection();
        if (ChatGlobal.isVariantAPI){
            ChatGlobal.isVariantAPI=false;
            con.isVariantApi=true;
        }
        String jsonString="server not working";
        try {
            Log.d(CustomTag,"VariantAsyncTask ----->>>> in try");
            con.token="id_token "+ChatGlobal.googleToken;
            jsonString = con.run(params[0]);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonString;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        Log.d(CustomTag, "in CartAsyncTask---------->>>>>  " + s);
        if (s.equalsIgnoreCase("server not working")) {
            util.setReplyMessage(ChatGlobal.mContext.getResources().getString(R.string.bot_try_later_reply));

        } else {
            String apiResponse = s;
            Log.e(CustomTag,"api response = "+apiResponse);
            //Getting Response from product list Api

           /* try {
                JSONObject obj = new JSONObject(apiResponse);
                JSONObject dataObj =obj.getJSONObject("Data");
                JSONObject cartDataObj =dataObj.getJSONObject("CartData");
                JSONArray cartArray = cartDataObj.getJSONArray("CartLines");
                Log.d(CustomTag,"cartArray.length = "+cartArray.length());
                for (int i=0;i<cartArray.length();i++) {
                    JSONObject itemObj = cartArray.getJSONObject(i);
                    String ProductId = itemObj.getString("ProductId");
                    Log.d(CustomTag,"productId = "+ProductId);
                }
                *//**
                 * edit here
                 * *//*

                Log.d(CustomTag,"Amount Due = "+cartDataObj.getString("AmountDue"));

            } catch (JSONException e) {
                e.printStackTrace();
            }*/
        }
    }
}
