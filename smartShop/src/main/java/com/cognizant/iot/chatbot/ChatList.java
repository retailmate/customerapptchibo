package com.cognizant.iot.chatbot;
/**
 * Activity for selection of Associate for Chat Support
 * */

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.cognizant.retailmate.R;


public class ChatList extends ActionBarActivity implements AdapterView.OnItemClickListener {

    ListView mChatList;
    String[] chatList={"Henry","Mark","Bill","Stuart"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chatbot_activity_chat_list);

        mChatList= (ListView) findViewById(R.id.listView_chat);
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,chatList);
        mChatList.setAdapter(adapter);
        setTitle("Select Associate");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        mChatList.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Log.e("TAG","onItemClick position = "+i);
//        TextView textView= (TextView) view;
        ChatGlobal.mRecipient=chatList[i];
        Intent intent=new Intent(ChatList.this,UserAssociateChat.class);
        startActivity(intent);
    }
}
