package com.cognizant.iot.chatbot.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.iot.chatbot.ChatGlobal;
import com.cognizant.iot.chatbot.ChatMain;

import com.cognizant.iot.chatbot.model.ProductDataModel;
import com.cognizant.retailmate.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by 543898 on 10/18/2016.
 */
public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ProductHolder> {
    private static final String CustomTag="CustomTag";

    private ArrayList<ProductDataModel> itemsList;
    private Context mContext;

    public ProductListAdapter(Context context, ArrayList<ProductDataModel> itemsList) {
        this.itemsList = itemsList;
        this.mContext = context;
    }

    @Override
    public ProductHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chatbot_product_single_card, null);
        ProductHolder mh = new ProductHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(ProductHolder holder, final int i) {

        ProductDataModel productData = itemsList.get(i);

        holder.tvTitle.setText(productData.getProductName());
        holder.priceView.setText(productData.getPrice()+"");

        if(ChatGlobal.online) {
            Picasso.with(mContext)
                    .load(ChatGlobal.imageBaseUrl + productData.getProductImageResource())
                    .placeholder(R.drawable.loading)
                    .error(R.drawable.loading)
                    .into(holder.itemImage);
        }else{
            holder.itemImage.setImageResource(productData.getImageSource());
        }
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(CustomTag, String.valueOf(ChatGlobal.tapToAdd));

                if (ChatGlobal.tapToAdd){

                    ChatGlobal.cartBuffer.append(itemsList.get(i).getProductName()+" added to cart.");
                    Log.d(CustomTag,"cartBuffer = "+ChatGlobal.cartBuffer.toString());
                    ChatGlobal.productID= itemsList.get(i).getProductId();
                    ChatMain.addTocart();
                    Log.e(CustomTag,itemsList.get(i).getProductId()+"********"+itemsList.get(i).getProductName());

                    //Toast.makeText(v.getContext(), tvTitle.getText()+ " added to cart", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(v.getContext(), itemsList.get(i).getProductName(), Toast.LENGTH_SHORT).show();
                }


            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }

    public class ProductHolder extends RecyclerView.ViewHolder {

        protected TextView tvTitle;

        protected ImageView itemImage;

        TextView priceView;

        LinearLayout linearLayout;




        public ProductHolder(final View view) {
            super(view);

            this.tvTitle = (TextView) view.findViewById(R.id.productName);
            this.itemImage = (ImageView) view.findViewById(R.id.productImage);
            this.priceView= (TextView) view.findViewById(R.id.productPrice);
            this.linearLayout= (LinearLayout) view.findViewById(R.id.product_singlr_card_layout);

        }

    }

}