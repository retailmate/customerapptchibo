package com.cognizant.iot.chatbot;

import android.os.AsyncTask;
import android.util.Log;

import com.cognizant.iot.chatbot.model.ProductDataModel;
import com.cognizant.retailmate.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by 540472 on 12/27/2016.
 */
public class ProductAsyncTask extends AsyncTask<String,Void,String> {

    private static final String CustomTag="CustomTag";
    ChatUtil util=new ChatUtil();

    @Override
    protected String doInBackground(String... params) {
        BaseConnection con = new BaseConnection();
        if (ChatGlobal.intentIsRecommend){
            con.isrecommend=true;
        }else if (ChatGlobal.intentIsAvailable || ChatGlobal.intentIsPrice|| ChatGlobal.intentIsAddToCart){
            con.isavailable=true;
        }
        con.token="id_token "+ChatGlobal.googleToken;
        String jsonString="server not working";
        try {
            jsonString = con.run(params[0]);

        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.e(CustomTag,"ProductAsyncTask ----------->>>> response received is "+ jsonString);
        return jsonString;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (s.equalsIgnoreCase("server not working")){
            util.setReplyMessage(ChatGlobal.mContext.getResources().getString(R.string.bot_try_later_reply));
        }else {
            String apiResponse = s;
            //Getting Response from product list Api
            try {
                JSONObject obj=new JSONObject(apiResponse);
                JSONArray productArray = (JSONArray) obj.get("value");
                ProductDataModel productDataModel;
                if (ChatGlobal.intentIsRecommend){
                    Log.d(CustomTag, "recommend---->>");
                    ChatGlobal.intentIsRecommend=false;
                    if (productArray.length()==0){
                        util.setReplyMessage(ChatGlobal.mContext.getResources().getString(R.string.bot_no_such_product_reply));
                    }else{
                        double low=0.0;
                        double high=0.0;

                        if (productArray.length()>0 ){
                            Log.d(CustomTag,"productArray.length()>0 ");
                            ChatGlobal.prevProductList.clear();
                            //filter according to price range
                            if (ChatGlobal.entityNumberList.size()==2){
                                Log.d(CustomTag,"entityNumberList.size()==2");
                                if (ChatGlobal.entityNumberList.get(0)<ChatGlobal.entityNumberList.get(1)){
                                    low=ChatGlobal.entityNumberList.get(0);
                                    high=ChatGlobal.entityNumberList.get(1);
                                }else{
                                    low=ChatGlobal.entityNumberList.get(1);
                                    high=ChatGlobal.entityNumberList.get(0);
                                }
                                Log.d(CustomTag,"low = "+low+"\nHigh = "+high );
                            }

                            for (int i=0;i<productArray.length();i++){
                                JSONObject itemObj = productArray.getJSONObject(i);
                                String name = itemObj.getString("ProductName");
                                double price = itemObj.getDouble("Custprice");
                                Log.d(CustomTag,"price = "+price);
                                String imageUrl= itemObj.getString("Image");
                                String productId = itemObj.getString("ProductId");
                                productDataModel=new ProductDataModel();
                                productDataModel.setProductName(name);
                                productDataModel.setPrice(price);
                                productDataModel.setProductImageResource(imageUrl);
                                productDataModel.setProductId(productId);

                                if (low!=0.0){
                                    if (price>=low && price<=high){
                                        ChatGlobal.productList.add(productDataModel);
                                        ChatGlobal.prevProductList.add(productDataModel);
                                        Log.d(CustomTag,"added to productList----> name = "+name);
                                    }
                                }else {
                                    ChatGlobal.productList.add(productDataModel);
                                    ChatGlobal.prevProductList.add(productDataModel);
                                }
                            }
                            if (ChatGlobal.productList.size()==0){
                                util.setReplyMessage(ChatGlobal.mContext.getResources().getString(R.string.bot_no_such_product_reply));
                            }else{
                                if (ChatGlobal.productList.size()==1){
                                    util.setReplyMessage(ChatGlobal.mContext.getResources().getString(R.string.bot_found_this_product_reply));
                                    util.setReplyProduct(ChatGlobal.productList);
                                }else{
                                    util.setReplyMessage(ChatGlobal.mContext.getResources().getString(R.string.bot_found_these_product_reply));
                                    util.setReplyProduct(ChatGlobal.productList);
                                }
                                util.setReplyMessage(ChatGlobal.mContext.getResources().getString(R.string.bot_ask_to_add_product_reply));
                                //askedAvailableToCart=true;
                                ChatGlobal.isAsked=true;
                            }
                            util.clearList();
                            Log.d(CustomTag,"------->>>>>After clearList()<<<<<--------");
                            Log.d(CustomTag,"prevProductList.size = "+ChatGlobal.prevProductList.size());
                        }
                    }
                }else if (ChatGlobal.intentIsAvailable){

                    Log.d(CustomTag,"asynkTask------> available");
                    ChatGlobal.intentIsAvailable=false;
                    if (productArray.length()==0){
                        util.setReplyMessage(ChatGlobal.mContext.getResources().getString(R.string.bot_no_such_product_reply));
                    }else{
                        double low=0.0;
                        double high=0.0;

                        if (productArray.length()>0 ){
                            Log.d(CustomTag,"productArray.length()>0 ");
                            ChatGlobal.prevProductList.clear();
                            //filter according to price range
                            if (ChatGlobal.entityNumberList.size()==2){
                                Log.d(CustomTag,"entityNumberList.size()==2");

                                if (ChatGlobal.entityNumberList.get(0)<ChatGlobal.entityNumberList.get(1)){
                                    low=ChatGlobal.entityNumberList.get(0);
                                    high=ChatGlobal.entityNumberList.get(1);
                                }else{
                                    low=ChatGlobal.entityNumberList.get(1);
                                    high=ChatGlobal.entityNumberList.get(0);
                                }
                                Log.d(CustomTag,"low = "+low+"\nHigh = "+high );
                            }

                            for (int i=0;i<productArray.length();i++){
                                JSONObject itemObj = productArray.getJSONObject(i);
                                String name = itemObj.getString("Name");
                                double price = itemObj.getDouble("Price");
                                String imageUrl= itemObj.getString("PrimaryImageUrl");
                                String recordID = itemObj.getString("RecordId");
                                productDataModel=new ProductDataModel();
                                productDataModel.setProductName(name);
                                productDataModel.setPrice(price);
                                productDataModel.setProductImageResource(imageUrl);
                                productDataModel.setProductId(recordID);

                                if (low!=0.0){
                                    if (price>=low && price<=high){
                                        ChatGlobal.productList.add(productDataModel);
                                        ChatGlobal.prevProductList.add(productDataModel);
                                        Log.d(CustomTag,"added to productList----> name = "+name);
                                    }
                                }else {
                                    ChatGlobal.productList.add(productDataModel);
                                    ChatGlobal.prevProductList.add(productDataModel);
                                }

                            }
                            if (ChatGlobal.productList.size()==0){
                                util.setReplyMessage(ChatGlobal.mContext.getResources().getString(R.string.bot_no_such_product_reply));
                            }else{
                                if (ChatGlobal.productList.size()==1){
                                    util.setReplyMessage(ChatGlobal.mContext.getResources().getString(R.string.bot_found_this_product_reply));
                                    util.setReplyProduct(ChatGlobal.productList);
                                }else{
                                    util.setReplyMessage(ChatGlobal.mContext.getResources().getString(R.string.bot_found_these_product_reply));
                                    util.setReplyProduct(ChatGlobal.productList);
                                }


                                // prevProductList=productList;
                                Log.d(CustomTag,"prevProductList.size = "+ChatGlobal.prevProductList.size());
                                util.setReplyMessage(ChatGlobal.mContext.getResources().getString(R.string.bot_ask_to_add_product_reply));
                                //askedAvailableToCart=true;
                                ChatGlobal.isAsked=true;
                            }
                            util.clearList();
                            Log.d(CustomTag,"------->>>>>After clearList()<<<<<--------");
                            Log.d(CustomTag,"prevProductList.size = "+ChatGlobal.prevProductList.size());

                        }

                    }

                }else if (ChatGlobal.intentIsPrice){

                    Log.d(CustomTag,"asynkTask------> price");
                    ChatGlobal.intentIsPrice=false;
                    if (productArray.length()==0){
                        util.setReplyMessage(ChatGlobal.mContext.getResources().getString(R.string.bot_no_such_product_reply));
                    }else{
                        double low=0.0;
                        double high=0.0;

                        if (productArray.length()>0 ){
                            Log.d(CustomTag,"productArray.length()>0 ");
                            ChatGlobal.prevProductList.clear();
                            //filter according to price range
                            if (ChatGlobal.entityNumberList.size()==2){
                                Log.d(CustomTag,"entityNumberList.size()==2");

                                if (ChatGlobal.entityNumberList.get(0)<ChatGlobal.entityNumberList.get(1)){
                                    low=ChatGlobal.entityNumberList.get(0);
                                    high=ChatGlobal.entityNumberList.get(1);
                                }else{
                                    low=ChatGlobal.entityNumberList.get(1);
                                    high=ChatGlobal.entityNumberList.get(0);
                                }
                                Log.d(CustomTag,"low = "+low+"\nHigh = "+high );
                            }


                            for (int i=0;i<productArray.length();i++){
                                JSONObject itemObj = productArray.getJSONObject(i);
                                String name = itemObj.getString("Name");
                                double price = itemObj.getDouble("Price");
                                String imageUrl= itemObj.getString("PrimaryImageUrl");
                                String recordID = itemObj.getString("RecordId");
                                productDataModel=new ProductDataModel();
                                productDataModel.setProductName(name);
                                productDataModel.setPrice(price);
                                productDataModel.setProductImageResource(imageUrl);
                                productDataModel.setProductId(recordID);

                                if (low!=0.0){
                                    if (price>=low && price<=high){
                                        ChatGlobal.productList.add(productDataModel);
                                        ChatGlobal.prevProductList.add(productDataModel);
                                        Log.d(CustomTag,"added to productList----> name = "+name);
                                    }
                                }else {
                                    ChatGlobal.productList.add(productDataModel);
                                    ChatGlobal.prevProductList.add(productDataModel);
                                }

                            }
                            if (ChatGlobal.productList.size()==0){
                                util.setReplyMessage(ChatGlobal.mContext.getResources().getString(R.string.bot_no_such_product_reply));
                            }else{
                                if (ChatGlobal.productList.size()==1){
                                    util.setReplyMessage(ChatGlobal.mContext.getResources().getString(R.string.bot_price_for_this_product_reply));
                                    util.setReplyProduct(ChatGlobal.productList);
                                }else{
                                    util.setReplyMessage(ChatGlobal.mContext.getResources().getString(R.string.bot_price_are_reply));
                                    util.setReplyProduct(ChatGlobal.productList);
                                }

                                Log.d(CustomTag,"prevProductList.size = "+ChatGlobal.prevProductList.size());
                                util.setReplyMessage(ChatGlobal.mContext.getResources().getString(R.string.bot_ask_to_add_product_reply));
                                ChatGlobal.isAsked=true;
                            }
                            util.clearList();
                            Log.d(CustomTag,"------->>>>>After clearList()<<<<<--------");
                            Log.d(CustomTag,"prevProductList.size = "+ChatGlobal.prevProductList.size());
                        }
                    }
                }else if (ChatGlobal.intentIsAddToCart){

                    Log.d(CustomTag,"asynkTask------> AddToCart");
                    ChatGlobal.intentIsAddToCart=false;
                    if (productArray.length()==0){
                        util.setReplyMessage(ChatGlobal.mContext.getResources().getString(R.string.bot_no_such_product_reply));
                    }else{

                        if (productArray.length()>0 ){
                            ChatGlobal.prevProductList.clear();

                            for (int i=0;i<productArray.length();i++){
                                JSONObject itemObj = productArray.getJSONObject(i);
                                String name = itemObj.getString("Name");
                                double price = itemObj.getDouble("Price");
                                String imageUrl= itemObj.getString("PrimaryImageUrl");
                                String recordID = itemObj.getString("RecordId");

                                productDataModel=new ProductDataModel();
                                productDataModel.setProductName(name);
                                productDataModel.setPrice(price);
                                productDataModel.setProductImageResource(imageUrl);
                                productDataModel.setProductId(recordID);

                                ChatGlobal.productList.add(productDataModel);
                                ChatGlobal.prevProductList.add(productDataModel);

                            }
                            if (ChatGlobal.productList.size()==0){
                                util.setReplyMessage(ChatGlobal.mContext.getResources().getString(R.string.bot_no_such_product_reply));
                            }else{
                                ChatGlobal.tapToAdd =true;
                                if (ChatGlobal.productList.size()==1){
                                    util.setReplyMessage(ChatGlobal.mContext.getResources().getString(R.string.bot_tap_on_product_to_confirm_reply));
                                }else{
                                    util.setReplyMessage(ChatGlobal.mContext.getResources().getString(R.string.bot_tap_on_products_to_confirm_reply));
                                }
                                util.setReplyProduct(ChatGlobal.productList);
                                Log.d(CustomTag,"prevProductList.size = "+ChatGlobal.prevProductList.size());
                            }
                            util.clearList();
                            Log.d(CustomTag,"------->>>>>After clearList()<<<<<--------");
                            Log.d(CustomTag,"prevProductList.size = "+ChatGlobal.prevProductList.size());
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            util.clearList();
        }
    }
}
