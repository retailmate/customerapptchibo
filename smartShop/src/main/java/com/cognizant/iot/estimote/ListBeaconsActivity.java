package com.cognizant.iot.estimote;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.cognizant.iot.model.AssetModel;
import com.cognizant.retailmate.R;
import com.estimote.sdk.Beacon;
import com.estimote.sdk.BeaconManager;
import com.estimote.sdk.Region;
import com.estimote.sdk.internal.utils.L;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListBeaconsActivity extends AppCompatActivity {

    private static final String TAG = ListBeaconsActivity.class.getSimpleName();
    private NotificationManager notificationManager;
    public static final String EXTRAS_TARGET_ACTIVITY = "extrasTargetActivity";
    public static final String EXTRAS_BEACON = "extrasBeacon";
    public static final String EXTRAS_BEACONPLUS = "extrasBeaconPLUS";

    private static final int REQUEST_ENABLE_BT = 1234;
    private static final Region ALL_ESTIMOTE_BEACONS_REGION = new Region("rid",
            null, null, null);

    private BeaconManager beaconManager;
    private LeDeviceListAdapter adapter;
    WakeLock wl;
    Boolean wish = false;
    Boolean alert = true;
    ArrayList<Beacon> newWishList;
    List<Beacon> newBeaconList;
    PopupMenu popup;
    /*
     * (non-Javadoc)
     *
     * @see android.app.Activity#onCreate(android.os.Bundle)
     *
     * EDIT
     */
    public static ArrayList<AssetModel> assetList = new ArrayList<AssetModel>();
    public static ArrayList<String> wishList = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        PowerManager pm = (PowerManager) getApplicationContext()
                .getSystemService(getApplicationContext().POWER_SERVICE);
        wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
        wl.acquire();
        notificationManager = (NotificationManager) this
                .getSystemService(Context.NOTIFICATION_SERVICE);
        super.onCreate(savedInstanceState);
        assetList = (ArrayList<AssetModel>) getIntent().getSerializableExtra(
                "list");

        wishList = (ArrayList<String>) getIntent().getSerializableExtra(
                "wishlist");

        newWishList = new ArrayList<Beacon>();
        newBeaconList = new ArrayList<Beacon>();

//        getActionBar().setHomeAsUpIndicator(R.drawable.back_aro);
        setContentView(R.layout.main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarBeaconList);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);


        // Configure device list.
        adapter = new LeDeviceListAdapter(this, assetList);
        ListView list = (ListView) findViewById(R.id.device_list);
        list.setAdapter(adapter);
//        list.setOnItemClickListener(createOnItemClickListener());

        // Configure verbose debug logging.
        L.enableDebugLogging(true);

        // Configure BeaconManager.
        beaconManager = new BeaconManager(this);
        beaconManager.setRangingListener(new BeaconManager.RangingListener() {
            @Override
            public void onBeaconsDiscovered(Region region,
                                            final List<Beacon> beacons) {
                // Note that results are not delivered on UI thread.
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // Note that beacons reported here are already sorted by
                        // estimated
                        // distance between device and beacon.
                        // getActionBar().setSubtitle(
                        // "Found beacons: " + beacons.size());
                        if (!wish) {
                            if (inBeaconList(beacons)) {
                                adapter.replaceWith(newBeaconList);
                            }
//                            adapter.replaceWith(inBeaconList(beacons));
                        } else {
                            if (inWhishList(beacons)) {
                                adapter.replaceWith(newWishList);

                            }
                        }
                    }

                });
            }
        });
    }

    private boolean inWhishList(List<Beacon> beacons) {
        // TODO Auto-generated method stub

        newWishList.clear();
        for (int i = 0; i < beacons.size(); i++) {
            for (int j = 0; j < wishList.size(); j++) {
                System.out.println("@@## beacons.get(i).getProximityUUID()"
                        + beacons.get(i).getProximityUUID());
                System.out.println("@@## wishList.get(j)" + wishList.get(j));
                if (beacons.get(i).getProximityUUID().equals(wishList.get(j))) {

                    if (!newWishList.contains(beacons.get(i))) {
                        System.out.println("@@## newWishList" + newWishList);
                        newWishList.add(beacons.get(i));
                    }

                }
            }

        }
        return true;
    }


    /*

    For checking beacons exisitng in the list
     */
    private Boolean inBeaconList(List<Beacon> beacons) {
        // TODO Auto-generated method stub

        newBeaconList.clear();
        for (int i = 0; i < beacons.size(); i++) {


            for (int j = 0; j < assetList.size(); j++) {

                if (beacons.get(i).getProximityUUID().toString().equals(assetList.get(j).getAssetBeaconId().toLowerCase())) {

                    if (!newBeaconList.contains(beacons.get(i))) {

                        newBeaconList.add(beacons.get(i));
                    }

                }
            }

        }
        return true;
    }


    private List<Beacon> offersortbeacons(List<Beacon> beacons) {
        // TODO Auto-generated method stub
        boolean exists = false;
        int count = 0;
        List<Integer> removearray = null;
        for (int j = 0; j < beacons.size(); j++) {
            for (int i = 0; i < assetList.size(); i++) {
                if (beacons.get(j).getProximityUUID().equals(assetList.get(i).getAssetBeaconId().toLowerCase())) {
                    exists = true;
                }
                if (i == assetList.size() - 1 && !exists) {
//                    removearray.add(j);
//                    Collections.swap(assetList, j, assetList.size());
                }
            }
        }

        return beacons;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.scan_menu, menu);
//        MenuItem refreshItem = menu.findItem(R.id.refresh);
//        refreshItem.setActionView(R.layout.actionbar_indeterminate_progress);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        switch (item.getItemId()) {
            case R.id.settings:

                popup = new PopupMenu(ListBeaconsActivity.this,
                        findViewById(R.id.settings));
                // Inflating the Popup using xml file
                if (wish && !alert) {
                    popup.getMenuInflater().inflate(R.menu.settings_menu_both,
                            popup.getMenu());
                } else if (!wish && !alert) {
                    popup.getMenuInflater().inflate(R.menu.settings_menu_notify,
                            popup.getMenu());
                } else if (wish && alert) {
                    popup.getMenuInflater().inflate(R.menu.settings_menu_wishlist,
                            popup.getMenu());
                } else {
                    popup.getMenuInflater().inflate(R.menu.settings_menu_none,
                            popup.getMenu());
                }
                // registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.notifications:
                                if (item.isChecked()) {
                                    item.setChecked(false);
                                    alert = true;

                                } else {
                                    item.setChecked(true);
                                    alert = false;
                                }

                                break;

                            case R.id.wishlist:

                                if (item.isChecked()) {
                                    item.setChecked(false);
                                    wish = false;

                                } else {
                                    item.setChecked(true);
                                    wish = true;
                                }

                                break;

                            default:
                                break;
                        }

                        return true;
                    }
                });

			/*
             * A hack to force icons, code! Some private method accessed
			 */

                Object menuHelper;
                Class[] argTypes;
                try {
                    Field fMenuHelper = PopupMenu.class.getDeclaredField("mPopup");
                    fMenuHelper.setAccessible(true);
                    menuHelper = fMenuHelper.get(popup);
                    argTypes = new Class[]{boolean.class};
                    menuHelper.getClass()
                            .getDeclaredMethod("setForceShowIcon", argTypes)
                            .invoke(menuHelper, true);
                } catch (Exception e) {

                    Log.w(TAG, "error forcing menu icons to show", e);
                    popup.show();
                    return true;
                }
            /*
             * END
			 */

//                popup.show();// showing popup menu

                // closing the setOnClickListener method

                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        beaconManager.disconnect();
        wl.release();
        notificationManager.cancelAll();
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();

//        // Check if device supports Bluetooth Low Energy.
//        if (!beaconManager.hasBluetooth()) {
//            Toast.makeText(this, "Device does not have Bluetooth Low Energy",
//                    Toast.LENGTH_LONG).show();
//            return;
//        }
//
//        // If Bluetooth is not enabled, let user enable it.
//        if (!beaconManager.isBluetoothEnabled()) {
//            Intent enableBtIntent = new Intent(
//                    BluetoothAdapter.ACTION_REQUEST_ENABLE);
//            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
//        } else {
        connectToService();
//        }
    }

    @Override
    protected void onStop() {
        beaconManager.stopRanging(ALL_ESTIMOTE_BEACONS_REGION);

        super.onStop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_OK) {
                connectToService();
            } else {
                Toast.makeText(this, "Bluetooth not enabled", Toast.LENGTH_LONG)
                        .show();
//                getActionBar().setSubtitle("Bluetooth not enabled");
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void connectToService() {
//        getActionBar().setSubtitle("Scanning...");
        adapter.replaceWith(Collections.<Beacon>emptyList());
        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                beaconManager.startRanging(ALL_ESTIMOTE_BEACONS_REGION);
            }
        });
    }

//    private AdapterView.OnItemClickListener createOnItemClickListener() {
//        return new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view,
//                                    int position, long id) {
//                if (getIntent().getStringExtra(EXTRAS_TARGET_ACTIVITY) != null) {
//                    try {
//                        Class<?> clazz = Class.forName(getIntent()
//                                .getStringExtra(EXTRAS_TARGET_ACTIVITY));
//                        Intent intent = new Intent(ListBeaconsActivity.this,
//                                clazz);
//                        intent.putExtra(EXTRAS_BEACON,
//                                adapter.getItem(position));
//                        intent.putExtra(EXTRAS_BEACONPLUS,
//                                adapter.getItem(position + 1));
//
//                        intent.putExtra("beaconDetails", assetList
//                                .get(position(adapter.getItem(position)
//                                        .getProximityUUID().toString())));
//                        intent.putExtra(
//                                "msg",
//                                assetList.get(
//                                        position(adapter.getItem(position)
//                                                .getProximityUUID().toString())).getOffers());
//                        startActivity(intent);
//                    } catch (ClassNotFoundException e) {
//                        Log.e(TAG, "Finding class by name failed", e);
//                    }
//                }
//            }
//
//            private int position(String macAddress) {
//                // TODO Auto-generated method stub
//                int i;
//                for (i = 0; i < assetList.size(); i++) {
//                    if (macAddress.equals(assetList.get(i).getAssetBeaconId())) {
//                        return i;
//                    }
//                }
//                return 2;
//            }
//        };
//    }
}
