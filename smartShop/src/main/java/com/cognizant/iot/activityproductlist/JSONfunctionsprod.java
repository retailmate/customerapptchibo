package com.cognizant.iot.activityproductlist;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.cognizant.iot.activity.CatalogActivity;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.iot.utils.MySSLSocketFactory;

public class JSONfunctionsprod {

    public static JSONArray getJSONfromAXURLCart(String url, String[] products) {
        InputStream is = null;
        String result = "";
        JSONArray jArray = null;

        // Download JSON data from URL
        try {

            HttpClient httpclient = MySSLSocketFactory.getNewHttpClient();
            HttpPost httppost = new HttpPost(url);

            List<NameValuePair> params = new ArrayList<NameValuePair>();

            String[] productIds = products;

            System.out.println("@@## productIds" + productIds[0]);

            // params.add(new BasicNameValuePair("productIds", Arrays
            // .toString(productIds)));
            for (int i = 0; i < productIds.length; i++) {
                params.add(new BasicNameValuePair(String.format(
                        "productIds[%d]", i), productIds[i]));
            }
            // params.add(new BasicNameValuePair("pass", "xyz"));
            // params.add(new BasicNameValuePair("pass", "xyz"));
            UrlEncodedFormEntity ent = new UrlEncodedFormEntity(params,
                    HTTP.UTF_8);
            httppost.setEntity(ent);

            HttpResponse response = httpclient.execute(httppost);
            HttpEntity httpentity = response.getEntity();
            is = httpentity.getContent();

        } catch (Exception e) {
            Log.e("log_tag", "Error in http connection " + e.toString());
        }

        // Convert response to string
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
        } catch (Exception e) {
            Log.e("log_tag", "Error converting result " + e.toString());
        }

        try {

            jArray = new JSONArray(result);
        } catch (JSONException e) {
            Log.e("log_tag", "Error parsing data " + e.toString());
        }

        return jArray;
    }

    /*
        Getting called for product List AX7 Latest
     */
    public static JSONObject getJSONfromAXURLGETProducts(String url) {
        InputStream is = null;
        String result = "";
        JSONObject jArray = null;

        // Download JSON data from URL
        try {

            HttpClient httpclient = MySSLSocketFactory.getNewHttpClient();
            HttpGet httpget = new HttpGet(url);

            httpget.addHeader("Authorization", "id_token " + AccountState.getTokenID());

            httpget.addHeader(
                    "OUN",
                    "094");

            HttpResponse response = httpclient.execute(httpget);
            HttpEntity httpentity = response.getEntity();
            is = httpentity.getContent();

        } catch (Exception e) {
            Log.e("log_tag", "Error in http connection " + e.toString());
        }

        // Convert response to string
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
        } catch (Exception e) {
            Log.e("log_tag", "Error converting result " + e.toString());
        }

        try {

            jArray = new JSONObject(result);
        } catch (JSONException e) {
            Log.e("log_tag", "Error parsing data " + e.toString());
        }

        return jArray;
    }

    public static JSONArray getJSONfromAXURL(String url) {
        InputStream is = null;
        String result = "";
        JSONArray jArray = null;

        // Download JSON data from URL
        try {

            HttpClient httpclient = MySSLSocketFactory.getNewHttpClient();
            HttpPost httppost = new HttpPost(url);

            List<NameValuePair> params = new ArrayList<NameValuePair>();

            String[] productIds = {"22565423865", "22565423115",
                    "22565423120", "22565423125", "22565423455", "22565423459",
                    "22565423464", "22565423940", "22565423947", "22565423954",
                    "22565424119", "22565424124", "22565424143", "22565423825",
                    "22565423835", "22565423855", "22565423678", "22565423700",
                    "22565423686", "22565423194", "22565423197", "22565423207",
                    "22565423732", "22565423737", "22565423748"};

            // params.add(new BasicNameValuePair("productIds", Arrays
            // .toString(productIds)));
            for (int i = 0; i < productIds.length; i++) {
                params.add(new BasicNameValuePair(String.format(
                        "productIds[%d]", i), productIds[i]));
            }
            // params.add(new BasicNameValuePair("pass", "xyz"));
            // params.add(new BasicNameValuePair("pass", "xyz"));
            UrlEncodedFormEntity ent = new UrlEncodedFormEntity(params,
                    HTTP.UTF_8);
            httppost.setEntity(ent);

            HttpResponse response = httpclient.execute(httppost);
            HttpEntity httpentity = response.getEntity();
            is = httpentity.getContent();

        } catch (Exception e) {
            Log.e("log_tag", "Error in http connection " + e.toString());
        }

        // Convert response to string
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
        } catch (Exception e) {
            Log.e("log_tag", "Error converting result " + e.toString());
        }

        try {

            jArray = new JSONArray(result);
        } catch (JSONException e) {
            Log.e("log_tag", "Error parsing data " + e.toString());
        }

        return jArray;
    }

    public static JSONObject getJSONfromURL(String url) {
        InputStream is = null;
        String result = "";
        JSONObject jArray = null;

        // Download JSON data from URL
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(url);

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("macaddr", CatalogActivity.imei));
            // params.add(new BasicNameValuePair("pass", "xyz"));
            // params.add(new BasicNameValuePair("pass", "xyz"));
            UrlEncodedFormEntity ent = new UrlEncodedFormEntity(params,
                    HTTP.UTF_8);
            httppost.setEntity(ent);

            HttpResponse response = httpclient.execute(httppost);
            HttpEntity httpentity = response.getEntity();
            is = httpentity.getContent();

        } catch (Exception e) {
            Log.e("log_tag", "Error in http connection " + e.toString());
        }

        // Convert response to string
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
        } catch (Exception e) {
            Log.e("log_tag", "Error converting result " + e.toString());
        }

        try {

            jArray = new JSONObject(result);
        } catch (JSONException e) {
            Log.e("log_tag", "Error parsing data " + e.toString());
        }

        return jArray;
    }

    public static JSONObject getJSONfromURLRecommendedProducts(String url) {
        InputStream is = null;
        String result = "";
        JSONObject jArray = null;

        // Download JSON data from URL
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(url);

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("UserID", AccountState.getUserID()));

            UrlEncodedFormEntity ent = new UrlEncodedFormEntity(params,
                    HTTP.UTF_8);
            httppost.setEntity(ent);

            HttpResponse response = httpclient.execute(httppost);
            HttpEntity httpentity = response.getEntity();
            is = httpentity.getContent();

        } catch (Exception e) {
            Log.e("log_tag", "Error in http connection " + e.toString());
        }

        // Convert response to string
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
        } catch (Exception e) {
            Log.e("log_tag", "Error converting result " + e.toString());
        }

        try {

            jArray = new JSONObject(result);
        } catch (JSONException e) {
            Log.e("log_tag", "Error parsing data " + e.toString());
        }

        return jArray;
    }

}
