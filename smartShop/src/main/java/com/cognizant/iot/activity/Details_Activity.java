/*package com.cognizant.iot.activity;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import android.app.ActionBar;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.iot.ar.AR_activity;
import com.cognizant.iot.ar.R;
import com.cognizant.iot.ar.imagerecognition.ImageRecognitionActivity;
import com.cognizant.iot.json.AssetsExtracter;
import com.cognizant.iot.model.AssetModel;

public class Details_Activity extends Activity {

	private static final String TAG = "Details";

	AssetModel assetmodel = new AssetModel();

	TextView assetName, priority, dateDesc, problemDesc, installationDate,
			brand, assetname_header;
	ImageView assetpic;
	AssetsExtracter mTask;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.details_layout);

		assetmodel = (AssetModel) getIntent().getSerializableExtra("assetData");

		System.out.println("model data" + assetmodel.getAssetName());

		assetName = (TextView) findViewById(R.id.assetName);
		priority = (TextView) findViewById(R.id.priority);
		dateDesc = (TextView) findViewById(R.id.dateDesc);
		problemDesc = (TextView) findViewById(R.id.probDesc);
		installationDate = (TextView) findViewById(R.id.installationDate);
		brand = (TextView) findViewById(R.id.brand);
		assetpic = (ImageView) findViewById(R.id.assetImage);

		priority.setText(assetmodel.getAssetPriority());
		dateDesc.setText("Issue raised on " + assetmodel.getAssetDate() + " , "
				+ assetmodel.getAssetTime() + "  Raised by "
				+ assetmodel.getContact());
		problemDesc.setText(assetmodel.getAssetProblemDesc());
		installationDate.setText(assetmodel.getAssetInstallationDate());
		brand.setText(assetmodel.getAssetBrand());
		assetName.setText(assetmodel.getAssetName());

		if (assetmodel.getAssetName().equals("TV Switch")) {
			assetpic.setImageDrawable(getResources().getDrawable(
					R.drawable.tv_switch_icon));
		} else if (assetmodel.getAssetName().equals("Heater")) {
			assetpic.setImageDrawable(getResources().getDrawable(
					R.drawable.heater_icon));
		} else {
			assetpic.setImageDrawable(getResources().getDrawable(
					R.drawable.user_icon));
		}
		if (assetmodel.getAssetPriority().equals("medium")) {
			priority.setTextColor(Color.YELLOW);
		} else {
			priority.setTextColor(Color.RED);
		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.actions, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
		case R.id.action_new:
			PopupMenu popup = new PopupMenu(Details_Activity.this,
					findViewById(R.id.action_new));
			// Inflating the Popup using xml file
			popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());

			// registering popup with OnMenuItemClickListener
			popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
				public boolean onMenuItemClick(MenuItem item) {
					Intent intent;
					switch (item.getItemId()) {
					case R.id.callexpert:
						// Toast.makeText(Details_Activity.this,
						// "You Clicked : " + item.getTitle(),
						// Toast.LENGTH_SHORT).show();
						intent = new Intent();
						intent.setComponent(new ComponentName(
								"com.android.contacts",
								"com.android.contacts.DialtactsContactsEntryActivity"));
						intent.setAction("android.intent.action.MAIN");
						intent.addCategory("android.intent.category.LAUNCHER");
						intent.addCategory("android.intent.category.DEFAULT");
						startActivity(intent);
						break;

					case R.id.arcapture:
						intent = new Intent(getApplicationContext(),
								AR_activity.class);
						startActivity(intent);

						break;
					case R.id.mail:
						intent = new Intent(Intent.ACTION_SEND);
						String[] recipients = { "serviceapp_supervisor1@gmail.com" };
						intent.putExtra(Intent.EXTRA_EMAIL, recipients);
						intent.putExtra(Intent.EXTRA_SUBJECT,
								"ServiceApp Subject");
						intent.putExtra(Intent.EXTRA_TEXT,
								"Service App Sample message");
						intent.putExtra(Intent.EXTRA_CC,
								"serviceapp_admin@gmail.com");
						intent.setType("text/html");
						startActivity(Intent.createChooser(intent, "Send mail"));
						startActivity(intent);

						break;
					case R.id.readinstructions:
						File file = new File(Environment
								.getExternalStorageDirectory()
								.getAbsolutePath()
								+ "/iotAR/docs/acocremoval.pdf");
						intent = new Intent(Intent.ACTION_VIEW);
						intent.setDataAndType(Uri.fromFile(file),
								"application/pdf");
						intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
						startActivity(intent);

						break;
					case R.id.imagerecognition:
						if (isNetworkAvailable()) {
							intent = new Intent(getApplicationContext(),
									ImageRecognitionActivity.class);
							startActivity(intent);
						} else {
							Toast.makeText(getApplicationContext(),
									"No network available", Toast.LENGTH_SHORT)
									.show();
						}

						break;
					default:
						break;
					}

					return true;
				}
			});

			
			 * A hack to force icons, code! Some private method accessed
			 

			Object menuHelper;
			Class[] argTypes;
			try {
				Field fMenuHelper = PopupMenu.class.getDeclaredField("mPopup");
				fMenuHelper.setAccessible(true);
				menuHelper = fMenuHelper.get(popup);
				argTypes = new Class[] { boolean.class };
				menuHelper.getClass()
						.getDeclaredMethod("setForceShowIcon", argTypes)
						.invoke(menuHelper, true);
			} catch (Exception e) {

				Log.w(TAG, "error forcing menu icons to show", e);
				popup.show();
				return true;
			}
			
			 * END
			 

			popup.show();// showing popup menu

			// closing the setOnClickListener method

			break;

		}
		return super.onOptionsItemSelected(item);
	}

	protected boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

}
*/