package com.cognizant.iot.extrascreens.models;

import java.io.Serializable;

/**
 * Created by Bharath on 11/04/17.
 */

public class ProductResponse implements Serializable {

    /**
     * productName : Gabby
     * description : Easy to carry ottoman , ideal for casual seating and storage space provided beneath the chair pad. Available in multiple color option.
     * itemCode : 1000003913433
     * color : White
     * material : ABS
     * dimensions : W 35 x L 35 x H 50
     * price : INR 4950
     */

    private String productName;
    private String description;
    private String itemCode;
    private String color;
    private String material;
    private String dimensions;
    private String price;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getDimensions() {
        return dimensions;
    }

    public void setDimensions(String dimensions) {
        this.dimensions = dimensions;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
