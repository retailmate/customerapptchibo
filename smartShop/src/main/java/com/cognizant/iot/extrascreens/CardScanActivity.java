package com.cognizant.iot.extrascreens;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.cognizant.iot.homepage.HomePageActivity;
import com.cognizant.iot.utils.AccountState;
import com.cognizant.retailmate.R;

public class CardScanActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main_scan);
    }

    public void proceedToNextActivtiy(View view) {


        AccountState.setUserNAME("John Legend");
        AccountState.setUserID("004021");
        Intent main_intent_catalog = new Intent(CardScanActivity.this,
                HomePageActivity.class);
        startActivity(main_intent_catalog);
    }
}
