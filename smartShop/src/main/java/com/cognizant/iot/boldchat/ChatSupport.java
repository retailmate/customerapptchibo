package com.cognizant.iot.boldchat;

import java.util.Date;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.boldchat.sdk.BoldChatSession;
import com.boldchat.sdk.BoldChatView;
import com.boldchat.visitor.api.ChatAvailabilityListener;
import com.boldchat.visitor.api.UnavailableReason;
import com.cognizant.retailmate.R;

public class ChatSupport extends AppCompatActivity {

	private BoldChatSession mBoldChat;
	Button mChatButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_chatsupport);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarChat);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_material);
		upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
		getSupportActionBar().setHomeAsUpIndicator(upArrow);

//		getActionBar().setHomeAsUpIndicator(R.drawable.back_aro);
//		getActionBar().setDisplayHomeAsUpEnabled(true);
//		getActionBar().setHomeButtonEnabled(true);
//		getActionBar().setDisplayShowTitleEnabled(true);
//		getActionBar().setBackgroundDrawable(
//				new ColorDrawable(Color.parseColor("#B5D000")));
//		getActionBar().setIcon(
//				new ColorDrawable(getResources().getColor(
//						android.R.color.transparent)));
//
//		int titleId = getResources().getIdentifier("action_bar_title", "id",
//				"android");
//
//		TextView abTitle = (TextView) findViewById(titleId);
//		abTitle.setTextColor(Color.parseColor("#ffffff"));

		mChatButton = (Button) findViewById(R.id.chat_button);

		inializeBoldChat();
		checkAvailability();
		mChatButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				inializeBoldChat();

				checkAvailability();
				startBoldChat();
				mChatButton.setVisibility(View.GONE);
			}
		});
	}

	long aID = 429474873135554201L;
	String apiKey = "429474873135554201:426385965140392995:DoeL5outOmeUVAfcQMVdaEyP/j6lKT5K";

	private void inializeBoldChat() {

		BoldChatView boldChatView = (BoldChatView) findViewById(R.id.boldchat);
		 mBoldChat = new BoldChatSession.Builder(ChatSupport.this, aID,
		 apiKey).build();

		mBoldChat = new BoldChatSession.Builder(boldChatView).build();

	}

	protected void stopBoldChat() {
		mBoldChat.removeListener();
	}

	private void checkAvailability() {
		mBoldChat.getChatAvailability(new ChatAvailabilityListener() {
			@Override
			public void onChatAvailable() {
				// This should be customized based on how you want to handle
				// chat being available.
				// mChatButton.setVisibility(View.VISIBLE);
			}

			@Override
			public void onChatUnavailable(UnavailableReason unavailableReason) {
				// This should be customized based on how you want to handle
				// chat being unavailable.
				mChatButton.setVisibility(View.GONE);
			}

			@Override
			public void onChatAvailabilityFailed(int failType, String message) {
				// This should be customized based on how you want to handle a
				// failure to determine chat availability
				onChatUnavailable(UnavailableReason.Unknown);
			}
		});
	}

	private void startBoldChat() {

		findViewById(R.id.boldchat).setVisibility(View.VISIBLE);

		mBoldChat.setListener(new BoldChatSession.BoldChatSessionListener() {
			@Override
			public void chatSessionCreated() {
			}

			@Override
			public void chatSessionStarted() {
			}

			@Override
			public void operatorTyping() {
			}

			@Override
			public void chatSessionEnded() {
			}

			@Override
			public void chatSessionClosed() {
				// This should be customized depending on how you want to handle
				// the close action.
				findViewById(R.id.boldchat).setVisibility(View.GONE);
				mChatButton.setVisibility(View.VISIBLE);
			}

			@Override
			public void messageArrived(String message, String sender, Date sent) {
				// TODO Auto-generated method stub

			}
		});
		mBoldChat.start();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		mBoldChat.addMenuItems(getMenuInflater(), menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (mBoldChat.menuItemSelected(item)) {
			return true;
		} else if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		} else {

			return super.onOptionsItemSelected(item);
		}

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		stopBoldChat();
	}

}
