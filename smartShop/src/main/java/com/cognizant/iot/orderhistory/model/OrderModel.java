package com.cognizant.iot.orderhistory.model;

/**
 * Created by 452781 on 12/21/2016.
 */


import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderModel implements Serializable {


    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("SalesId")
    @Expose
    private String salesId;
    @SerializedName("OrderStatus")
    @Expose
    private String orderStatus;
    @SerializedName("CustAccount")
    @Expose
    private String custAccount;
    @SerializedName("InvoiceId")
    @Expose
    private String invoiceId;
    @SerializedName("DlvMode")
    @Expose
    private String dlvMode;
    @SerializedName("DateCreated")
    @Expose
    private String dateCreated;
    @SerializedName("Datedelivered")
    @Expose
    private String datedelivered;
    @SerializedName("Items")
    @Expose
    private List<OrderDetailModel> items = null;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public String getSalesId() {
        return salesId;
    }

    public void setSalesId(String salesId) {
        this.salesId = salesId;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getCustAccount() {
        return custAccount;
    }

    public void setCustAccount(String custAccount) {
        this.custAccount = custAccount;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getDlvMode() {
        return dlvMode;
    }

    public void setDlvMode(String dlvMode) {
        this.dlvMode = dlvMode;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getDatedelivered() {
        return datedelivered;
    }

    public void setDatedelivered(String datedelivered) {
        this.datedelivered = datedelivered;
    }

    public List<OrderDetailModel> getItems() {
        return items;
    }

    public void setItems(List<OrderDetailModel> items) {
        this.items = items;
    }

}
