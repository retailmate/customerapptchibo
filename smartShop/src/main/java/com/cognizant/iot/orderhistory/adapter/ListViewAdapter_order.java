package com.cognizant.iot.orderhistory.adapter;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cognizant.iot.orderhistory.activity.Order_Products;
import com.cognizant.iot.orderhistory.activity.Order_Screen;
import com.cognizant.iot.orderhistory.model.OrderModel;
import com.cognizant.retailmate.R;

public class ListViewAdapter_order extends BaseAdapter {

    // Declare Variables
    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, ArrayList>> data;

    HashMap<String, ArrayList> resultp = new HashMap<String, ArrayList>();
    ProgressDialog mProgressDialog;
    JSONObject jsonobject;
    JSONArray jsonarray;

    ArrayList<HashMap<String, String>> arraylist;
    String prodid1;

    Double ordertotaltosend;

    OrderModel[] orderModel;

//    public ListViewAdapter_order(Context context,
//                                 ArrayList<HashMap<String, ArrayList>> arraylist) {
//        this.context = context;
//        data = arraylist;
//
//    }

    public ListViewAdapter_order(Context context,
                                 OrderModel[] orderModel) {
        this.context = context;
        this.orderModel = orderModel;

    }

    @Override
    public int getCount() {
        return orderModel.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        // Declare Variables
        TextView orderid;
        TextView orderdate;
        TextView orderstatus;
        TextView orderprice;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(R.layout.listview_item_order, parent,
                false);
        // Get the position
//        resultp = data.get(position);

        double orderTotal = 0.0;

        final OrderModel orderModelsingle = orderModel[position];

        orderid = (TextView) itemView.findViewById(R.id.orderid_main);
        orderdate = (TextView) itemView.findViewById(R.id.order_date);
        orderstatus = (TextView) itemView.findViewById(R.id.order_status_main1);
        orderprice = (TextView) itemView.findViewById(R.id.order_price_main);

        final ArrayList<HashMap<String, String>> productData;

//        productData = resultp.get("invoice");

//        System.out.println("@@## InvoiceId productData" + resultp.get("invoice"));

//        orderid.setText(productData.get(0).get(Order_Screen.ORDERID));
//        orderid.setText("INV-" + productData.get(0).get("salesid"));
        orderid.setText("INV-" + orderModelsingle.getSalesId());

//        System.out.println("@@## InvoiceId productData" + productData.get(0).get(Order_Screen.ORDERID));
//        orderdate.setText(productData.get(0).get(Order_Screen.ORDERDATE));
//        orderdate.setText(orderModelsingle.getDateCreated());


        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd'T'HH:mm:ss");


            Date newdate = sdf.parse(orderModelsingle.getDateCreated());

            SimpleDateFormat sdftwo = new SimpleDateFormat("dd/MM/yyyy");
            orderdate.setText(sdftwo.format(newdate));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < orderModelsingle.getItems().size(); i++) {
//            System.out.println("@@## ORDER PRICE INDIVIDUAL" + productData.get(i).get("prodprice"));
            orderTotal = orderTotal + orderModelsingle.getItems().get(i).getLineAmount();
        }

        ordertotaltosend = orderTotal;

        orderprice.setText("$" + ((int) orderTotal));


        itemView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // Get the position
//                resultp = data.get(position);
                Intent intent = new Intent(context, Order_Products.class);

                intent.putExtra("OrderData", (Serializable) orderModelsingle);
                System.out.println("@@## Order Screen passing total" + ordertotaltosend);
                intent.putExtra("OrderPrice", ordertotaltosend);


                context.startActivity(intent);

            }
        });

        return itemView;
    }

}