package com.cognizant.iot.utils;

/**
 * Created by 429023 on 8/11/2016.
 */
public class Constants {
    public static String REGISTRATION_REQUEST = "registration_request";
    public static String REGISTRATION_CATEGORY_REQUEST = "registration_category_request";

    public static String REGISTRATION_ENDPOINT = "http://lmcustomervalidationax.azurewebsites.net/api/Customer/CustomerList?idToken=";
    public static String REGISTRATION_CATEGORY_ENDPOINT = "http://rmlmapi.azurewebsites.net/api/CustomerRegistration/CustomerRegistrationAPI";
    public static String CART_ADD_ITEM_REQUEST = "http://lmcustomervalidationax.azurewebsites.net/api/cart/CreateCart?idToken=";
    public static String CART_LIST_ITEM_REQUEST = "http://lmcustomervalidationax.azurewebsites.net/api/cart/GetCart?idToken=";
    public static String GET_PRODUCT_BY_IDS = "https://landmarkdevret.cloudax.dynamics.com/Commerce/Products/SearchByText(channelId=0,catalogId=0,searchText='%s')?$top=20&api-version=7.1";
    public static String CART_REMOVE_ITEM_REQUEST = "http://lmcustomervalidationax.azurewebsites.net/api/cart/RemoveCartItems?idToken=";


    public static String WISHLIST_ITEM_REQUEST = "http://lmcustomervalidationax.azurewebsites.net/api/WishList/GetWishList?idToken=";
    public static String WISHLIST_ADD_ITEM_REQUEST = "http://lmcustomervalidationax.azurewebsites.net/api/WishList/CreateWishList?idtoken=";
    public static String WISHLIST_REMOVE_ITEM_REQUEST = "http://lmcustomervalidationax.azurewebsites.net/api/WishList/DeleteWishListItems?idToken=";


    public static String GET_ORDERS_HISTORY_REQUEST = "http://rmlmapi.azurewebsites.net/api/Order/OrderHistoryAPI";

    public static String CREATE_PURCHASE_ORDER = "http://rmlmapi.azurewebsites.net/api/JDAServices/CreateCustomerOrderAPI";


    public static String RECOMMENDATION_API = "http://rmlmapi.azurewebsites.net/api/CognitiveServices/GetUserToItemRecommendationsAPI";
    public static String PRODUCT_SEARCH_BY_ID_API = "https://landmarkdevret.cloudax.dynamics.com/Commerce/Products/Search?$top=250&api-version=7.1";
    public static String PRODUCT_CATEGORY_API = "https://landmarkdevret.cloudax.dynamics.com/Commerce/Categories/GetCategories?$top=250&api-version=7.1";
    public static String PRODUCT_LIST_BY_CATEGORY_API = "https://landmarkdevret.cloudax.dynamics.com/Commerce/Products/SearchByCategory(channelId=68719476778,catalogId=0,categoryId=";


    public static String SignalRURL = "http://rmlmapi.azurewebsites.net/";
    public static String SignalRHubProxy = "LMHubServer";
}
