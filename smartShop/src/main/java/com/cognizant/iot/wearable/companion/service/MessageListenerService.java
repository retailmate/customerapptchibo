package com.cognizant.iot.wearable.companion.service;

import java.util.List;
import java.util.concurrent.TimeUnit;

import android.util.Log;
import android.widget.Toast;

import com.cognizant.iot.wearable.companion.Cart;
import com.cognizant.iot.wearable.companion.Constants;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.data.FreezableUtils;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

public class MessageListenerService extends WearableListenerService {
	private static final String TAG = "@@##";
	private static final int NOTIFICATION_ID = 100;

	private GoogleApiClient mGoogleApiClient;

	@Override
	public void onCreate() {
		super.onCreate();
		mGoogleApiClient = new GoogleApiClient.Builder(this).addApi(
				Wearable.API).build();
		mGoogleApiClient.connect();
	}

	@Override
	public void onDataChanged(DataEventBuffer dataEvents) {
		final List<DataEvent> events = FreezableUtils
				.freezeIterable(dataEvents);
		dataEvents.close();

		if (!mGoogleApiClient.isConnected()) {
			ConnectionResult connectionResult = mGoogleApiClient
					.blockingConnect(30, TimeUnit.SECONDS);
			if (!connectionResult.isSuccess()) {
				Log.e(TAG,
						"MOBILE:: Service failed to connect to GoogleApiClient.");
				return;
			}
		}

		for (DataEvent event : events) {
			if (event.getType() == DataEvent.TYPE_CHANGED) {
				String path = event.getDataItem().getUri().getPath();
				Log.d(TAG, "MOBILE:: path: " + path);
				if (path.equals("")) {

				} else {
					Log.d(TAG, "MOBILE:: Unrecognized path: " + path);
				}
			}
		}
	}

	@Override
	public void onMessageReceived(MessageEvent messageEvent) {
		Log.d(TAG, "MOBILE:: onMessageReceived messageEvent path: "
				+ messageEvent.getPath());

		
		
		if (messageEvent.getPath().equals(Constants.PATH_SERVER_REQUEST)) {
			Toast.makeText(getBaseContext(), "Start service",
					Toast.LENGTH_SHORT).show();

			System.out.println("\n \n @@## RECEIVED REQUEST FROM WATCH \n \n");
			// add item to cart
			Cart cart = new Cart(getBaseContext());
			cart.addItemToCart();
			

		} else {
			Toast.makeText(getBaseContext(), "Unknown request",
					Toast.LENGTH_SHORT).show();
		}
	}
}
