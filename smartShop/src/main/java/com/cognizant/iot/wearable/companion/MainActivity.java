package com.cognizant.iot.wearable.companion;

import java.io.ByteArrayOutputStream;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.cognizant.iot.wearable.companion.service.BeaconService;
import com.cognizant.retailmate.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.Asset;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

public class MainActivity extends Activity implements
		GoogleApiClient.ConnectionCallbacks,
		GoogleApiClient.OnConnectionFailedListener {

	private static final String TAG = "@@##";
	private static GoogleApiClient mGoogleApiClient;
	private static int count = 0;
	static Context context = null;
	Intent intent;
	SharedPreferences pref;

	String url;
	String deviceToken;
	String appVersion;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = getBaseContext();
		setContentView(R.layout.activity_main);

		intent = getIntent();
		if (intent != null) {
			url = intent.getStringExtra("server_domain");
			deviceToken = intent.getStringExtra("device_token");
			appVersion = intent.getStringExtra("app_version");

			pref = context.getApplicationContext().getSharedPreferences(
					"MyPref", 0);
			Editor editor = pref.edit();
			editor.putString("url", url);
			editor.putString("deviceToken", deviceToken);
			editor.putString("appVersion", appVersion);
			editor.commit();
			Log.d(TAG, "url = " + url + ", deviceToken = " + deviceToken
					+ ", appVersion = " + appVersion);
		} else {
			Log.d(TAG, "intent is null");
		}

		Log.d("@@##", "BeaconService going to start soon ");
		getBaseContext().startService(
				new Intent(getBaseContext(), BeaconService.class));

		findViewById(R.id.send_button).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						// sendMessage("");

						// Discover discover = new Discover(getBaseContext());
						// discover.getCardSummary();
					}
				});

		findViewById(R.id.dismiss_button).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						dismissNotification();
					}
				});

		mGoogleApiClient = new GoogleApiClient.Builder(this)
				.addApi(Wearable.API).addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this).build();

		if (!mGoogleApiClient.isConnected()) {
			mGoogleApiClient.connect();
		}
	}

	// @Override
	// public boolean onCreateOptionsMenu(Menu menu) {
	// // Inflate the menu; this adds items to the action bar if it is present.
	// getMenuInflater().inflate(R.menu.main, menu);
	// return true;
	// }
	//
	// @Override
	// public boolean onOptionsItemSelected(MenuItem item) {
	// // Handle action bar item clicks here. The action bar will
	// // automatically handle clicks on the Home/Up button, so long
	// // as you specify a parent activity in AndroidManifest.xml.
	// int id = item.getItemId();
	// if (id == R.id.action_settings) {
	// return true;
	// }
	// return super.onOptionsItemSelected(item);
	// }

	@Override
	public void onDestroy() {
		if (mGoogleApiClient.isConnected()) {
			mGoogleApiClient.disconnect();
		}

		super.onDestroy();
	}

	public static void sendMessage(String response, String path) {
		if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
			PutDataMapRequest putDataMapRequest = PutDataMapRequest
					.create(path);

			// Add data to the request
			// putDataMapRequest.getDataMap().putString(Constants.KEY_TITLE,
			// String.format("hello world! %d", count++));
			putDataMapRequest.getDataMap().putString(Constants.KEY_TITLE,
					response);
			putDataMapRequest.getDataMap().putString(Constants.KEY_TIMESTAMP,
					String.valueOf(System.currentTimeMillis()));

			Bitmap icon = BitmapFactory.decodeResource(context.getResources(),
					R.drawable.retail_mate_app_icon);
			Asset asset = createAssetFromBitmap(icon);
			putDataMapRequest.getDataMap().putAsset(Constants.KEY_IMAGE, asset);

			PutDataRequest request = putDataMapRequest.asPutDataRequest();
			Log.d(TAG, "sending response data to wear");
			Wearable.DataApi.putDataItem(mGoogleApiClient, request)
					.setResultCallback(
							new ResultCallback<DataApi.DataItemResult>() {
								@Override
								public void onResult(
										DataApi.DataItemResult dataItemResult) {
									Log.d(TAG,
											"response data sent :: putDataItem status: "
													+ dataItemResult
															.getStatus()
															.toString());
								}
							});
		} else {
			Log.d(TAG, "mGoogleApiClient not connected  ");
		}
	}

	private void dismissNotification() {
		if (mGoogleApiClient.isConnected()) {
			new AsyncTask<Void, Void, Void>() {
				@Override
				protected Void doInBackground(Void... params) {
					NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi
							.getConnectedNodes(mGoogleApiClient).await();
					for (Node node : nodes.getNodes()) {
						MessageApi.SendMessageResult result = Wearable.MessageApi
								.sendMessage(mGoogleApiClient, node.getId(),
										Constants.PATH_DISMISS, null).await();
						if (!result.getStatus().isSuccess()) {
							Log.e(TAG, "ERROR: failed to send Message: "
									+ result.getStatus());
						}
					}

					return null;
				}
			}.execute();
		}
	}

	@Override
	public void onConnected(Bundle bundle) {
	}

	@Override
	public void onConnectionSuspended(int i) {
	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		Log.e(TAG, "Failed to connect to Google Api Client with error code "
				+ connectionResult.getErrorCode());
	}

	private static Asset createAssetFromBitmap(Bitmap bitmap) {
		final ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteStream);
		return Asset.createFromBytes(byteStream.toByteArray());
	}

}
