package com.cognizant.iot.wearable.companion.model;

import java.util.List;

public class Alerts {
	private List<Alert> alerts;
	
	public List<Alert> getAlerts() {
		return alerts;
	}

	public void setAlerts(List<Alert> alerts) {
		this.alerts = alerts;
	}

}
