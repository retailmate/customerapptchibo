package com.cognizant.iot.model;

import java.io.Serializable;

public class AssetModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */

	String assetName;
	String assetBeaconId;
	String assetTime;
	String assetDate;
	String assetDesc;
	String url;
	String assetBrand;
	String distance;
	String offers;
	Boolean notfied;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Boolean getNotfied() {
		return notfied;
	}

	public void setNotfied(Boolean notfied) {
		this.notfied = notfied;
	}

	public String getOffers() {
		return offers;
	}

	public void setOffers(String offers) {
		this.offers = offers;
	}

	public String getDistance() {
		System.out.println("IN MODEL " + distance);
		return distance;

	}

	public void setDistance(String distance) {
		this.distance = distance;
		System.out.println("IN MODEL " + distance);
	}

	public String getAssetName() {
		System.out.println("Getting Asset in Model " + assetName);
		return assetName;

	}

	public void setAssetName(String assetName) {
		this.assetName = assetName;

	}

	public String getAssetTime() {
		return assetTime;
	}

	public void setAssetTime(String assetTime) {
		this.assetTime = assetTime;
	}

	public String getAssetDate() {
		return assetDate;
	}

	public void setAssetDate(String assetDate) {
		this.assetDate = assetDate;
	}

	public String getAssetBrand() {
		return assetBrand;
	}

	public void setAssetBrand(String assetBrand) {
		this.assetBrand = assetBrand;
	}

	public String getAssetBeaconId() {
		return assetBeaconId;
	}

	public void setAssetBeaconId(String assetBeaconId) {
		this.assetBeaconId = assetBeaconId;
	}

	public String getAssetDesc() {
		return assetDesc;
	}

	public void setAssetDesc(String assetDesc) {
		this.assetDesc = assetDesc;
	}

}
