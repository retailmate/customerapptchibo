package com.cognizant.iot.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 452781 on 1/24/2017.
 */
public class CustomerGeoLocationModel {

    @SerializedName("CustomerId")
    @Expose
    private Integer customerId;
    @SerializedName("AssociateId")
    @Expose
    private Integer associateId;
    @SerializedName("StoreName")
    @Expose
    private String storeName;
    @SerializedName("Distance")
    @Expose
    private String distance;
    @SerializedName("CaptureTime")
    @Expose
    private String captureTime;

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Integer getAssociateId() {
        return associateId;
    }

    public void setAssociateId(Integer associateId) {
        this.associateId = associateId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getCaptureTime() {
        return captureTime;
    }

    public void setCaptureTime(String captureTime) {
        this.captureTime = captureTime;
    }
}
